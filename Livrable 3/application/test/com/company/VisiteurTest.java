package com.company;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class VisiteurTest {

    Visiteur dummy ;

    @Before
    public void setUp() throws Exception {
        dummy = new Visiteur("Test","Dummy", "dummyTest@gmail.com","1234567890","2021-12-06");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void basicStoreInfo() {
        assertEquals("Visiteur=Test=Dummy=dummyTest@gmail.com=1234567890=2021-12-06=none=none",dummy.storeInfo());
    }

    @Test public void AjoutProfilVaccinaux(){
        ProfilVaccination pV = new ProfilVaccination("000000000001","0101-10-01", 1,"NotARealOne", "0101010101");
        dummy.ajouterProfilsVaccinaux(pV);
        assertEquals(1, dummy.getProfilsVaccinaux().size());
    }
}