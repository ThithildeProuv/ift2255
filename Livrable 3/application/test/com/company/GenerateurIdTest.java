package com.company;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GenerateurIdTest {
    GenerateurId instance_GenerateurId;
    @Before
    public void setUp() throws Exception{
        instance_GenerateurId = GenerateurId.getInstance();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void initialisationGenerateurVide(){
        assertArrayEquals(new int[] {0,0,0},
                instance_GenerateurId.getLongueurGenerateur());
    }

    @Test
    public void nouvelIdDansLesListes(){
        String newId = instance_GenerateurId.generateIdUtilisateur();
        assertTrue(instance_GenerateurId.idsContains(newId));
    }

    @Test
    public void ajoutNouvelIdDansListe(){
        int lenDebut = instance_GenerateurId.getLongueurGenerateur()[0];
        instance_GenerateurId.generateIdUtilisateur();
        int lenFin = instance_GenerateurId.getLongueurGenerateur()[0];
        assertEquals(lenDebut+1, lenFin);
    }

    @Test
    public void lengthIdsCodes(){
        String newIdUtilisateur = instance_GenerateurId.generateIdUtilisateur();
        String newCodeUtilisateur = instance_GenerateurId.generateCodeUtilisateur();
        String newIdRendezVous = instance_GenerateurId.generateIdRendezVous();
        assertArrayEquals(new int[] {12,9,6},
                new int[]{newIdUtilisateur.length(),newCodeUtilisateur.length(),newIdRendezVous.length()});
    }
}
