package com.company;

import com.company.Employe;
import com.company.Visiteur;
import com.company.Benevole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeTest {
    Employe testEmploye;
    Visiteur testVisiteur;
    Benevole testBenevole;
    Visiteur testVisiteur2;
    Benevole testBenevole2;

    @Before
    public void setUp() throws Exception {
        testEmploye = new Employe("Leblanc", "Blanche","blb@hotmail.com",
                "4506676677","rRth67&","223 rue Laval", "H2R4Y3", "Montreal");

        testVisiteur = new Visiteur("Lerouge", "Robert", "rlr@gmail.com",
                "4165565566", "1923-03-07");

        testBenevole = new Benevole("Legris", "Marguerite", "mlg@yahoo.fr",
                "6183343232", "MDp56^T", "43 rue Fleury", "A2D5T5",
                "Ottawa", "");

        // Prenom devient Marcel
        testVisiteur2 = new Visiteur("Lerouge", "Marcel", "rlr@gmail.com",
                "4165565566", "1923-03-07");

        // Mot de passe devient ABcd98*9
        testBenevole2 = new Benevole("Legris", "Marguerite", "mlg@yahoo.fr",
                "6183343232", "ABcd98*9", "43 rue Fleury", "A2D5T5",
                "Ottawa", "");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void creerCompteVisiteur() {
        assertArrayEquals(testVisiteur.getAccountInfo(), (testEmploye.creerCompteVisiteur("Lerouge",
               "Robert", "rlr@gmail.com", "4165565566",
                "1923-03-07")).getAccountInfo());
    }

    @Test
    public void creerCompteEmploye() {
        assertArrayEquals(testEmploye.getAccountInfo(), (testEmploye.creerCompteEmploye("Leblanc", "Blanche",
                "blb@hotmail.com", "4506676677","rRth67&",
                "223 rue Laval", "H2R4Y3", "Montreal")).getAccountInfo());
    }

    @Test
    public void creerCompteBenevole() {
        assertArrayEquals(testBenevole.getAccountInfo(), (testEmploye.creerCompteBenevole("Legris", "Marguerite",
                "mlg@yahoo.fr", "6183343232", "MDp56^T",
                "43 rue Fleury", "A2D5T5", "Ottawa", "")).getAccountInfo());
    }

    @Test
    public void modifierCompteVisi() {
        testEmploye.modifierCompteVisi(testVisiteur,"prenom", "Marcel");
        assertArrayEquals(testVisiteur2.getAccountInfo(), testVisiteur.getAccountInfo());
    }

    @Test
    public void modifierCompteBenevole() {
        testEmploye.modifierCompteBenevole(testBenevole,"mot de passe", "ABcd98*9");
        assertArrayEquals(testBenevole2.getAccountInfo(), testBenevole.getAccountInfo());
    }
}