# README

Cette application est un prototype de VaxTodo permettant de prendre des rendez-vous et gérer des comptes.

## Fonctionnalités

- Prendre un rendez-vous pour la vaccination
- Gérer les comptes des visiteurs, bénévoles et des employés
- Envoyer les courriels de profil vaccinal

## Manuel d'utilisation

Pour utiliser l'application, il vous faut exécuter les commandes suivantes :

``` bash
java -jar VaxToDoRe.jar
```

Il se peut que nous ayons un problème dans le `jar`... Si c'est le cas, essayez avec un IDEA.

### Menu principal (Employé)

À partir du menu principal, dans le rôle de l'employé, vous pouvez choisir l'une des options suivantes en tapant le chiffre correspondant.
En tout tant vous pouvez taper 0 pour revenir au menu principal.

- [q] Quitter
- [1] Menu réservation
- [2] Menu vaccination
- [3] Menu gestion de compte

### Menu compte

Dans cette section, vous pouvez effectuer les actions suivantes en tapant le chiffre correspondant.
Suivez les instructions à l'écran pour compléter la tache

- [1] Créer un compte
- [2] Modifier un compte
- [3] Supprimer un compte
- [4] Consulter un compte

### Menu réservation

- [1] Prendre un rendez-vous
- [2] Confirmer un rendez-vous
- [3] Modifier/annuler un rendez-vous
- [4] Retour au menu principal

### Menu vaccination

- [1] Question vaccination
- [2] Profil Vaccinal
- [3] Retour au menu principal

## Données

Les données des utilisateurs sont partillement enregistré dans database.txt. Celui-ci est
géré par la classe Database. Le outputFilePath indique l'endroit ou le ficher txt seras crée.

Par default, il est crée dans le projet.

Le fichier garde, les utiliateurs de session, malheureusement le calendrier n'est pas enregistré,
mais biensûre, dans un autre context, on aurait un serveur ou tout structures de données mieux adapté
à ces fins.