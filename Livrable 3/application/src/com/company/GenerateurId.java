package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class GenerateurId {
    private static final GenerateurId instance = new GenerateurId();
    private final LinkedList<Integer> idsUtilisateurs;
    private final LinkedList<Integer> codesUtilisateurs;
    private final LinkedList<Integer> idsRendezVous;
    private static final int lenIdUtilisateur = 12;
    private static final int lenCodeUtilisateur = 9;
    private static final int lenIdRendezVous = 6;

    private GenerateurId(){
        this.idsUtilisateurs = new LinkedList<>();
        this.codesUtilisateurs = new LinkedList<>();
        this.idsRendezVous = new LinkedList<>();
    }

    /**
     * Singleton : on ne veut qu'une instance
     * @return l'instance du générateur d'Id, pour que la classe soit instanciée une seule fois
     */
    public static GenerateurId getInstance() {
        if(instance==null)
            return new GenerateurId();
        return instance;
    }

    /**
     * Génère un id unique pour un compte utilisateur
     * @return un id d'utilisateur (12 caractères) unique (non utilisé avant)
     */
    public String generateIdUtilisateur(){
        Random random = new Random();
        int newIdInt = random.nextInt((int)Math.pow(10,lenIdUtilisateur)-1);
        while (this.idsUtilisateurs.contains(newIdInt)){
            newIdInt = random.nextInt((int)Math.pow(10,lenIdUtilisateur)-1);
        }
        String newId = padLeftZeros(newIdInt,lenIdUtilisateur);
        this.idsUtilisateurs.add(newIdInt);
        return newId;
    }

    /**
     * Génère un code d'identification unique pour un compte de personnel
     * @return un code d'identification (9 caractères) unique (non utilisé avant)
     */
    public String generateCodeUtilisateur(){
        Random random = new Random();
        int newIdInt = random.nextInt((int)Math.pow(10,lenCodeUtilisateur)-1);
        while (this.codesUtilisateurs.contains(newIdInt)){
            newIdInt = random.nextInt((int)Math.pow(10,lenCodeUtilisateur)-1);
        }
        String newId = padLeftZeros(newIdInt,lenCodeUtilisateur);
        this.codesUtilisateurs.add(newIdInt);
        return newId;
    }

    /**
     * Génère un id unique pour un rendez-vous
     * @return un id de rendez-vous (6 caractères) unique (non utilisé avant)
     */
    public String generateIdRendezVous(){
        Random random = new Random();
        int newIdInt = random.nextInt((int)Math.pow(10,lenIdRendezVous)-1);
        while (this.idsRendezVous.contains(newIdInt)){
            newIdInt = random.nextInt((int)Math.pow(10,lenIdRendezVous)-1);
        }
        String newId = padLeftZeros(newIdInt,lenIdRendezVous);
        this.idsRendezVous.add(newIdInt);
        return newId;
    }

    /**
     * Ajoute un id d'utilisateur déjà utilisé (pour ne pas le réutiliser plus tard)
     * @param idUtilisateur string de 12 chiffres
     */
    public void addIdUtilisateur(String idUtilisateur){
        int newIdInt = Integer.parseInt(idUtilisateur);
        this.idsUtilisateurs.add(newIdInt);
    }

    /**
     * Ajoute un code d'identification de personnel déjà utilisé (pour ne pas le réutiliser plus tard)
     * @param codeUtilisateur string de 9 chiffres
     */
    public void addCodeUtilisateur(String codeUtilisateur){
        int newIdInt = Integer.parseInt(codeUtilisateur);
        this.codesUtilisateurs.add(newIdInt);
    }
    /**
     * Ajoute un id de rendez-vous déjà utilisé (pour ne pas le réutiliser plus tard)
     * @param idRendezVous string de 6 chiffres
     */
    public void addIdRendezVous(String idRendezVous){
        int newIdInt = Integer.parseInt(idRendezVous);
        this.idsRendezVous.add(newIdInt);
    }


    /**
     * ajoute des '0' à gauche jusqu'à atteindre la bonne longueur de string
     * @param id string où il faut ajouter les 0
     * @param len taille de string attendue
     * @return une string de la bonne taille
     */

    private String padLeftZeros(Integer id, int len){
        StringBuilder resultat = new StringBuilder(id.toString());
        while(resultat.length() < len)
            resultat.insert(0, '0');
        return resultat.toString();
    }

    /**
     * Pour les tests, affiche le nombre de numeros uniques déjà utilisés
     * @return [len(ids),len(codes),len(rdv)]
     */
    public int[] getLongueurGenerateur(){
        return new int[] {idsUtilisateurs.size(),codesUtilisateurs.size(),idsRendezVous.size()};
    }


    /**
     * Pour les tests, vérifie qu'un id d'utilisateur est déjà utilisé
     * @param id id d'utilisateur déjà utilisé
     * @return true si l'id est déjà utilisé, false sinon
     */

    public boolean idsContains(String id){
        int idInt = Integer.parseInt(id);
        return this.idsUtilisateurs.contains(idInt);
    }

}
