package com.company;

import java.time.LocalDateTime;
import java.util.LinkedList;

public class PlageHoraire {
    private final LocalDateTime dateDebut;
    private final LocalDateTime dateFin;
    private final LinkedList<RendezVous> listeRendezVous = new LinkedList<>();
    private final LinkedList<RendezVous> listeVisiteSpontanee = new LinkedList<>();
    private static final int nbVisiteursMax = 15;

    /**
     * Une plage horaire est une date et heure qui contient une liste de rendez-vous
     * @param dateDebut jour et heure du début de la plage horaire
     */
    PlageHoraire( LocalDateTime dateDebut){
        this.dateDebut = dateDebut;
        this.dateFin = dateDebut.plusHours(1);
    }

    public boolean valider72H(){
        return true;
    }

    public boolean validerMaxVisiteur(){
        return true;
    }

    public boolean validerMax2personne(){
        return true;
    }

    public boolean validerMax40personne(){
        return true;
    }

    public boolean validerDateHeure(){
        return true;
    }

    /**
     * représentation d'une plage horaire
     * @return une string avec : l'heure de la plage horaire
     */
    public String toString(){
        String result = this.dateDebut.toString()+","+this.dateFin.toString();
        return result;
    }

    /**
     * représentation d'une plage horaire avec sa liste de rendez-vous
     * @return une string avec : l'heure et la liste des rendez-vous de la plage horaire
     */
    public String showRendezVous(){
        String result = this.dateDebut.toString() + " " + listeRendezVous.toString();
        return result;
    }

    /**
     * représentation d'une plage horaire avec son nombre de places disponibles
     * @return une string avec : l'heure et le nombre de places disponibles
     */
    public String showNbPlaces(){
        String result = this.dateDebut.toString();
        int nbPlaces = nbVisiteursMax - this.listeRendezVous.size() - this.listeVisiteSpontanee.size();
        return result + " " + Integer.toString(nbPlaces) + " places disponibles";
    }

    /**
     * test d'égalité avec une autre plage horaire
     * @param horaire string qui contient un horaire
     * @return true si l'horaire correspond à l'horaire de cette plage horaire; false sinon
     */
    public boolean isEquals(String horaire){
        return LocalDateTime.parse(horaire).equals(this.dateDebut);
    }

    /**
     * cherche un rendez-vous dans la liste des rendez-vous de cette plage horaire
     * @param numeroRendezVous string du numéro du rendez-vous
     * @return le rendez-vous recherché, null si on ne l'a pas
     */
    public RendezVous getRendezVous(String numeroRendezVous){
        for (RendezVous rdv : listeRendezVous){
            if (rdv.equals(numeroRendezVous)){
                return rdv;
            }
        }
        return null;
    }

    /**
     * enlève un rendez-vous
     * @param numeroRendezVous string du numéro du rendez-vous à supprimer
     */
    public void supprimerRendezVous(String numeroRendezVous){
        RendezVous rendezVous = this.getRendezVous(numeroRendezVous);
        this.listeRendezVous.remove(rendezVous);
    }

    /**
     * ajoute un rendez-vous
     * @param rendezVous rendez-vous à ajouter
     */
    public void addRendezVous(RendezVous rendezVous){
        this.listeRendezVous.add(rendezVous);
    }
}


