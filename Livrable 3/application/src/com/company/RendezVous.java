package com.company;

public class RendezVous {
    final String numeroRendezVous;
    int typeDose;
    String nom;
    String prenom;
    String courriel;
    PlageHoraire plageHoraire;
    boolean isConfirme = false;

    RendezVous (int typeDose, String nom, String prenom, String courriel, PlageHoraire plageHoraire){
        this.typeDose = typeDose;
        this.nom = nom;
        this.prenom = prenom;
        this.courriel = courriel;
        this.plageHoraire = plageHoraire;

        this.numeroRendezVous = Main.generateurId.generateIdRendezVous();
    }



    public String storeInfo(){
        return getNumeroRendezVous()+","+getTypeDose()+","+getPlageHoraire().toString()+","+isConfirme;
    }

    public String getNumeroRendezVous() {
        return numeroRendezVous;
    }

    public int getTypeDose() {
        return typeDose;
    }

    public void setTypeDose(int typeDose) {
        this.typeDose = typeDose;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCourriel() {
        return courriel;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public PlageHoraire getPlageHoraire() {
        return plageHoraire;
    }

    public void setPlageHoraire(PlageHoraire plageHoraire) {
        this.plageHoraire = plageHoraire;
    }

    public boolean isConfirme() {
        return isConfirme;
    }

    public void setConfirme(boolean confirme) {
        isConfirme = confirme;
    }

    public String toString(){
        return numeroRendezVous + ", de " + nom + " " + prenom + " le " + plageHoraire.toString();
    }



    public boolean equals(String numeroRendezVous){
        return this.numeroRendezVous.equals(numeroRendezVous);
    }

    /**
     * pour envoyer le détail à la prise du rendez-vous
     */
    public void envoyerDetailsRendezVous(){ /* envoyer par courriel*/ }

    /**
     * pour confirmer le rendez-vous à l'arrivée du visiteur
     */
    public void confirmer(){ this.isConfirme = true; }
}
