package com.company;


public class Personnel extends Utilisateur{
    /**
     * Classe Personnel est un super-type pour les classes benevole et employe, qui permet de centraliser
     * les informations communes aux deux classes. Elle est un sous-type d'utilisateur.
     */
    private final String codeIdentification;
    private String motDePasse;
    private String address;
    private String codePostal;
    private String ville;

    /**
     * Passe les informations a la superclasse et genere un code d'identification unique pour les benvoles et employes
     * @param nom nom
     * @param prenom prenom
     * @param courriel courriel
     * @param numTelephone numero a dix chiffres
     * @param typeCompte type de compte donne par les sous-classes
     * @param mdp mot de passe
     * @param add adresse
     * @param cp code poste
     * @param ville ville de residence
     */
    Personnel(String nom, String prenom, String courriel, String numTelephone, com.company.Utilisateur.TypeCompte
            typeCompte, String mdp, String add, String cp, String ville){
        super(nom, prenom, courriel, numTelephone, typeCompte);

        this.codeIdentification = Main.generateurId.generateCodeUtilisateur();
        this.motDePasse = mdp;
        this.address = add;
        this.codePostal = cp;
        this.ville = ville;
    }

    /**
     * Constructeur utilise par la base de donnees.
     */
    Personnel(String nom, String prenom, String courriel, String numTelephone, TypeCompte typeCompte,
              String mdp, String add, String cp, String ville, String id, String codeIdentification){
        super(nom, prenom, courriel, numTelephone, typeCompte, id);
        this.codeIdentification = codeIdentification;

        this.motDePasse = mdp;
        this.address = add;
        this.codePostal = cp;
        this.ville = ville;
    }

     /**
     * Formattage des informations pour la consultation
     * @return une string pour montrer les informations
     */
    public String showInfo(){
        return "|#Unique: "+getNumUnique()+"|\n|Nom: "+getNom()+"|\n|Prenom: "+getPrenom()+"|\n|Courriel: "+getCourriel()+
                "|\n|#Telephone: "+getNumTelephone()+"|\n|Mot de pass: "+getMotDePasse()+"|\n|Adresse: "+getAddress()+
                "|\n|Code postal: "+getCodePostal()+"|\n|Ville: "+getVille()+"|";
    };

    /**
     * Implementee par les sous-classes
     */
    @Override
    public String storeInfo() {
        return null;
    }

    /**
     * Implementee par les sous-classes
     */
    @Override
    public String[] getAccountInfo() {return null;}

    /**
     * Obtient le type de compte a partir de la superclasse
     * @return type de compte
     */
    public com.company.Utilisateur.TypeCompte getTypeCompte() {
        return super.getTypeCompte();
    }

    public String getCodeIndification() {
        return codeIdentification;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Reimplementee par les sous-classes
     */
    public String toString(){return "";}
}
