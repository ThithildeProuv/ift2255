package com.company;

import java.awt.*;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Main {
    static GenerateurId generateurId = GenerateurId.getInstance();
    static DataBase dbUtilisateur  = new DataBase();
    static HashMap<String, Utilisateur> comptDict = dbUtilisateur.load(generateurId);

    static Calendrier calendrier = new Calendrier(LocalDate.now(),7);


    public static void main(String[] args) {
        System.out.println(comptDict);
        Employe currentEmploye = new Employe("Agere", "Ella","ellagere@gmail.com",
                "1234567", "Vaxtodo", "123 rue Despin",
                "1a1 a1a", "Montreal");

        InterfaceMenu menu = new InterfaceMenu(currentEmploye);
        menu.afficherMenuPrincipal();
    }
}
