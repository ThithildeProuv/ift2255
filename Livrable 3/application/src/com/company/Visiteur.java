package com.company;

import java.util.LinkedList;

public class Visiteur extends Utilisateur{
    /**
     * Représente les visiteurs, qui sont des utilisateurs du système avec une date de naissance et un choix de vaccin.
     */
    private String dateNaissance;
    private LinkedList<RendezVous> visites = new LinkedList<>();
    private LinkedList<ProfilVaccination> profilsVaccinaux = new LinkedList<ProfilVaccination>();

    /**
     * Objet visiteur, sous-type d'utilisateur. Passe le type de compte VISITEUR a la super-classe.
     * @param nom nom du visiteur
     * @param prenom prenom du visiteur
     * @param courriel courriel du visiteur
     * @param numTelephone numero de dix chiffres
     * @param dateNaissance AAAA-MM-JJ
     */
    public Visiteur(String nom, String prenom, String courriel, String numTelephone,
                    String dateNaissance){
        super(nom, prenom, courriel, numTelephone, TypeCompte.VISITEUR);
        this.dateNaissance = dateNaissance;
    }

    /**
     * Constructeur utilise par la BD.
     */
    public Visiteur(String nom, String prenom, String courriel, String numTelephone,
                    String dateNaissance,String numUnique){
        super(nom, prenom, courriel, numTelephone, TypeCompte.VISITEUR, numUnique);
        this.dateNaissance = dateNaissance;
    }

    /**
     * Formattage des informations pour la consultation avec des espaces
     * @return String des informations
     */
    public String toString(){
      return "|#Unique: "+getNumUnique()+"| Nom: "+getNom()+"| Prenom: "+getPrenom()+"| Courriel: "+getCourriel()+
              "| #Telephone: "+getNumTelephone()+"| Date de naissance: "+getDateNaissance()+
              "| Profils vaccinaux: "+getProfilsVaccinaux().size();
    };

    /**
     * Formattage des informations pour la consultation
     * @return une string pour montrer les informations
     */
    public String showInfo(){
        return "|#Unique: "+getNumUnique()+"|\n|Nom: "+getNom()+"|\n|Prenom: "+getPrenom()+"|\n|Courriel: "+getCourriel()+
                "|\n|#Telephone: "+getNumTelephone()+"|\n|Date de naissance: "+getDateNaissance()+
                "|\n|Profils vaccinaux: "+getProfilsVaccinaux().size();
    };

    /**
     * Formatte l'objet dans le format utilise par la BD
     * @return String formatte
     */
    public String storeInfo(){

        String infoVacc = "none";
        String infoRendezVous = "none";
        if(profilsVaccinaux.size() != 0){
            infoVacc = "";
            for (ProfilVaccination pv : profilsVaccinaux){
                infoVacc += pv.storeInfo()+"& ";
            }
        }
        if(visites.size() !=0){
            infoRendezVous = "";
            for(RendezVous rv : visites){
                infoRendezVous += rv.storeInfo()+"& ";
            }
        }

        return "Visiteur="+getNom()+"="+getPrenom()+"="+getCourriel()+
                "="+getNumTelephone()+"="+getDateNaissance()+"="+infoVacc+"="+infoRendezVous;
    };

    public String getDateNaissance() {
        return dateNaissance;
    }

    /**
     * Pour les tests, retourne les informations liees au compte visiteur sans l'identifiant unique
     * @return informations sans ID unique
     */
    public String[] getAccountInfo() {
        String[] infos = {getNom(), getPrenom(), getCourriel(), getNumTelephone(),getDateNaissance()};
        return infos;
    }

    public void setDateNaissance(String nouvelleDateNaissance) {
        this.dateNaissance = nouvelleDateNaissance;
    }

    public LinkedList<RendezVous> getVisites() {
        return visites;
    }

    // Ajoute une visite suite a un rendez-vous de vaccination
     /**
     * Ajoute une visite suite a un rendez-vous de vaccination
     * @param visite objet Visite
      */
    public void ajouterVisite(RendezVous visite) {
        visites.add(visite);
    }

    /**
     * Nombre de visites ne devrait pas depasser 2 (2 doses max pour le vaccin)
     */
    public boolean validerMaxVisites() {
        return (visites.size() < 2);
    }

    public LinkedList<ProfilVaccination> getProfilsVaccinaux() {
        return profilsVaccinaux;
    }

    /**
     * Ajoute un profil vaccinal suite a un rendez-vous de vaccination
     * @param profilVaccinal
     */
    public void ajouterProfilsVaccinaux(ProfilVaccination profilVaccinal) {
        profilsVaccinaux.add(profilVaccinal);
    }
}
