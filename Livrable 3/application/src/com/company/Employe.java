package com.company;

/*
* La classe Employé conserve les informations de l'utilisateur du système, en plus d'informations personnelles.
* */
public class Employe extends Personnel{

    /**
     * Employe est un type de personnel avec la possibilite de modifier/creer/supprimer des comptes
     * d'utilisateurs.
     * @param nom nom de famille
     * @param prenom prenom
     * @param courriel courriel
     * @param numTelephone numero de 10 chiffres
     * @param motDePasse Maj, min, chiffre et caractere special
     * @param adresse Adresse de residence
     * @param codePostal six caracteres
     * @param ville ville de residence
     */
    public Employe(String nom, String prenom, String courriel, String numTelephone,
           String motDePasse, String adresse, String codePostal,
            String ville){
        super(nom, prenom, courriel, numTelephone, TypeCompte.EMPLOYE, motDePasse, adresse,
                codePostal, ville);
    }

    /**
     * Permet a un employe de creer un nouveau compte pour un Visiteur
     * @param nom nom de famille du visiteur
     * @param prenom prenom du visiteur
     * @param courriel courriel du visiteur
     * @param numTelephone numero a dix chiffres
     * @param dateNaissance AAAA-MM-JJ
     * @return l'objet Visiteur cree
     */
    public Visiteur creerCompteVisiteur(String nom, String prenom, String courriel, String numTelephone,
                                        String dateNaissance){
        Visiteur nouveauVisiteur = new Visiteur(nom, prenom, courriel, numTelephone,dateNaissance);

        return nouveauVisiteur;
    }

    /**
     * Permet a un employe de creer un nouveau compte pour un Employe
     * @param nom nom de famille du nouvel employe
     * @param prenom prenom du nouvel employe
     * @param courriel courriel du nouvel employe
     * @param numTelephone numero a dix chiffre
     * @param motDePasse Maj, min, chiffre et caractere special
     * @param adresse adresse du nouvel employe
     * @param codePostal six caracteres
     * @param ville ville du nouvel employe
     * @return nouvel objet employe
     */
    public Employe creerCompteEmploye(String nom, String prenom, String courriel, String numTelephone,
                                      String motDePasse, String adresse, String codePostal, String ville) {
        Employe nouveauEmploye = new Employe(nom, prenom, courriel, numTelephone, motDePasse, adresse, codePostal, ville);

        return nouveauEmploye;
    }

    /**
     * Permet a un employe de creer un nouveau compte pour un Benevole
     * @param nom nom du benevole
     * @param prenom prenom du benevole
     * @param courriel courriel du benevole
     * @param numTelephone dix chiffres
     * @param motDePasse Maj, min, caractere special et chiffre
     * @param adresse adresse du benevole
     * @param codePostal six caracteres
     * @param ville ville de residence du benevole
     * @param dispo disponibilite (AAAA-MM-JJ)
     * @return le nouvel objet Benevole
     */
    public Benevole creerCompteBenevole(String nom, String prenom, String courriel, String numTelephone,
                                        String motDePasse, String adresse, String codePostal, String ville, String dispo){
        Benevole nouveauBenevole = new Benevole(nom, prenom, courriel, numTelephone, motDePasse, adresse, codePostal, ville, dispo);

        return  nouveauBenevole;
    }

    /**
     * A partir du numero unique associe au compte, l'employe peut supprimer ce compte
     * @param numUnique numero unique associe au compte
     */
    public void supprimerCompte(String numUnique){
        Main.comptDict.remove(numUnique);
    }

    /**
     * Modifie un compte de type Visiteur
     * @param visiteur Objet auquel le compte est associe
     * @param ancien nom de l'information qui doit etre modifiee
     * @param nouveau valeur avec laquelle l'information doit etre remplacee
     */
    public void modifierCompteVisi(Visiteur visiteur, String ancien, String nouveau){
       switch (ancien){
           case "nom" -> visiteur.setNom(nouveau);
           case "prenom" -> visiteur.setPrenom(nouveau);
           case "courriel" -> visiteur.setCourriel(nouveau);
           case "#telephone" -> visiteur.setTelephone(nouveau);
           case "date de naissance" -> visiteur.setDateNaissance(nouveau);
           default -> System.out.println("La modification s'effectue sur un argument qui n'existe pas...");
       }
    }

    /**
     * Modifie un compte de type Benevole
     * @param benevole Objet auquel le compte est associe
     * @param ancien nom de l'information qui doit etre modifiee
     * @param nouveau valeur avec laquelle l'information doit etre remplacee
     */
    public void modifierCompteBenevole(com.company.Benevole benevole, String ancien, String nouveau){
        switch (ancien){
            case "nom" -> benevole.setNom(nouveau);
            case "prenom" -> benevole.setPrenom(nouveau);
            case "courriel" -> benevole.setCourriel(nouveau);
            case "#telephone" -> benevole.setTelephone(nouveau);
            case "mot de passe" -> benevole.setMotDePasse(nouveau);
            case "adresse" -> benevole.setAddress(nouveau);
            case "code postal" -> benevole.setCodePostal(nouveau);
            case "ville" -> benevole.setVille(nouveau);
            case "disponibilite" -> benevole.setDisponibilite(nouveau);
            default -> System.out.println("La modification s'effectue sur un argument qui n'existe pas...");
        }
    }

    /**
     * Formatte les donnes pour afficher les employes
     * @return string formattee
     */
    public String toString(){
     return ""+getNom()+" "+getPrenom()+" E";
    }

    /**
     * Pour les tests, retourne les informations liees au compte employe sans l'identifiant unique
     * @return informations sans ID unique
     */
    public String[] getAccountInfo() {
        String[] infos = {getNom(), getPrenom(), getCourriel(), getNumTelephone(),
                getMotDePasse(), getAddress(), getCodePostal(), getVille()};
        return infos;
    }
}







