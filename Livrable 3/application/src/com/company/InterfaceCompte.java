package com.company;

import java.util.Map;
import java.util.Scanner;

public class InterfaceCompte {
    /**
     * Menu permettant de gerer (creer, supprimer, modifier, consulter) les comptes des visiteurs,
     * des employes et des benevoles.
     */
    private Employe courant;
    private InterfaceMenu menu;
    private com.company.Formulaire formulaire = com.company.Formulaire.getInstance();

    InterfaceCompte(Employe employe, InterfaceMenu menu){
        this.courant = employe;
        this.menu = menu;
    }

    /**
     * Menu principal pour choisir le type de compte (visiteur, employe ou benevole) et le
     * type d'action a effectuer sur le compte.
     */
    public void principalCompte(){
        java.lang.System.out.println("\n   ---  Menu Compte ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("1: Gestion Visiteur");
        java.lang.System.out.println("2: Gestion Employe");
        java.lang.System.out.println("3: Gestion Benevole");
        System.out.println("4: Retour au menu principal");
        String choixType = scanner.nextLine();

        Utilisateur.TypeCompte tc;
        switch (choixType){
            case "1" -> tc = Utilisateur.TypeCompte.VISITEUR;
            case "2" -> tc = Utilisateur.TypeCompte.EMPLOYE;
            case "3" -> tc = Utilisateur.TypeCompte.BENEVOLE;
            default -> tc = null;
        }

        if(choixType.equals("4")){menu.afficherMenuPrincipal();}

        if(tc == null){
            System.out.println("Erreur");
            principalCompte();
        }

        java.lang.System.out.println("1: Ajouter");
        java.lang.System.out.println("2: Modifier");
        java.lang.System.out.println("3: Supprimer");
        System.out.println("4: Consulter le compte d'un individus");
        System.out.println("5: Consulter la liste de comptes");
        String choixAction = scanner.nextLine();

        switch (choixAction){
            case "1" -> creerCompte(tc);
            case "2" -> modifierCompte(tc);
            case "3" -> supprimerCompte(tc);
            case "4" -> consulterCompte();
            case "5" -> consulterListeCompte(tc);
            default -> System.out.println("L'entrée n'est pas un option");
        }
    }

    /**
     * Menu de creation de compte, les informations specifiques au type de compte cree sont reccueillies.
     * @param tc type de compte devant etre cree (employe, visiteur ou benevole)
     */
    public void creerCompte(Utilisateur.TypeCompte tc){
        System.out.println("\n   ---   Création d'un compte   ---   \n");

        String prenom = formulaire.getInfoFormulaire("prénom", 50);
        String nom = formulaire.getInfoFormulaire("nom", 50);
        String cellNumber = formulaire.getNumeroFormulaire("telephone", 10);
        String courriel = formulaire.getInfoFormulaire("courriel",100);

        if(tc == Utilisateur.TypeCompte.VISITEUR){
            String birthday = formulaire.getDateFormulaire("naissance");

            Visiteur nVisiteur = courant.creerCompteVisiteur(nom,prenom,courriel,cellNumber, birthday);
            Main.comptDict.put(nVisiteur.getNumUnique(),nVisiteur);
            Main.dbUtilisateur.save(Main.comptDict);
            System.out.println(nVisiteur.showInfo());
        }

        if(tc == Utilisateur.TypeCompte.BENEVOLE){

            String mdp = formulaire.getMotDePasseFormulaire();
            String adresse = formulaire.getInfoFormulaire("adresse civile", 100);
            String codePostal = formulaire.getNumCharFormulaire("code postal", 6);
            String ville = formulaire.getInfoFormulaire("ville", 50);
            String dispo = formulaire.getDateFormulaire("dispo");

            Benevole nBenevole = courant.creerCompteBenevole(nom, prenom, courriel, cellNumber,
                    mdp, adresse,codePostal,ville,dispo);

            Main.comptDict.put(nBenevole.getNumUnique(), nBenevole);
            Main.dbUtilisateur.save(Main.comptDict);
            System.out.println("Votre numero de personnel est: "+nBenevole.getCodeIndification());
            System.out.println(nBenevole.showInfo());
            System.out.println(Main.comptDict);
        }



    }

    /**
     * Supprime un compte a partir du numero de compte a 12 chiffres.
     * @param tc le type de compte a supprimer
     */
    private void supprimerCompte(Utilisateur.TypeCompte tc) {
        System.out.println("\n   ---   Menu de Supression d'un compte   ---   ");
        String numUniqueSupp = formulaire.getNumeroFormulaire("compte", 12);

        if(!Main.comptDict.containsKey(numUniqueSupp)){
            System.out.println("Ce numero de compte n'existe pas");
            return;
        }
        if(tc != Main.comptDict.get(numUniqueSupp).getTypeCompte()){
            System.out.println("Ce numero de compte n'est pas associe a un visiteur");
            return;
        }

        infoCompte(tc, numUniqueSupp);
        String valide = formulaire.getQuestionsOuiNon("Etes-vous certain.e de vouloir supprimer " +
                "le compte ci-dessus (oui/non)");

        if (valide.equals("oui")) {
            courant.supprimerCompte(numUniqueSupp);
            System.out.println("Le compte a ete supprimé avec succes");
        } else {
            principalCompte();
            return;
        }
        System.out.println(Main.comptDict);
    }

    /**
     * Methode qui permet de modifier le compte selon son type
     * @param tc type de compte (benevole, employe, visiteur)
     */
    private void modifierCompte(Utilisateur.TypeCompte tc) {
        System.out.println("   ---   Menu de modification   ---   ");
        System.out.println("Entrer le numero unique du compte a modifier");
        String numUniqueMod = formulaire.getNumeroFormulaire("compte", 12);

        if(!Main.comptDict.containsKey(numUniqueMod)){
            System.out.println("Ce compte n'existe pas dans le repertoire");
            principalCompte();
        } else {
            infoCompte(tc, numUniqueMod);
            String[] infos = formulaire.getInfomartionModifiee();
            System.out.println(String.format("%s et %s",infos[0], infos[1]));
            if (infos == null) {
                return;
            }
            if(tc == Utilisateur.TypeCompte.VISITEUR){
                courant.modifierCompteVisi((Visiteur) Main.comptDict.get(numUniqueMod), infos[0], infos[1]);
                System.out.println(((Visiteur) Main.comptDict.get(numUniqueMod)).showInfo());
            }

            if(tc == Utilisateur.TypeCompte.EMPLOYE){
                return;
            }

            if(tc == Utilisateur.TypeCompte.BENEVOLE){
                courant.modifierCompteBenevole((Benevole) Main.comptDict.get(numUniqueMod),  infos[0], infos[1]);
                System.out.println(((Benevole) Main.comptDict.get(numUniqueMod)).showInfo());
                Main.dbUtilisateur.save(Main.comptDict);
            }

            System.out.println("Modification effectuee avec succes");
        }
    }

    /**
     * Permet de consulter un compte a partir du courriel et de la date de naissance ou du numero unique
     * d'identification.
     */
    public void consulterCompte(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Rechercher par...(Courriel et Date/#Unique)");
        String recherche = scanner.nextLine();
        if(recherche.equals("Courriel et Date")){
            String courriel = formulaire.getInfoFormulaire("courriel", 100);
            String ddn = formulaire.getDateFormulaire("naissance");

            for (Map.Entry<String,Utilisateur> entry : Main.comptDict.entrySet()){
                Utilisateur utilisateur = entry.getValue();
                if(utilisateur.getTypeCompte() == Utilisateur.TypeCompte.VISITEUR){
                    Visiteur visiteur = (Visiteur) utilisateur;
                    if(visiteur.getDateNaissance().equals(ddn) && visiteur.getCourriel().equals(courriel)){
                        infoCompte(visiteur.getTypeCompte(), visiteur.getNumUnique());
                    }
                }
            }
        }
        if(recherche.equals("#Unique")){
            String numUnique  = formulaire.getNumeroFormulaire("compte", 12);

            if(!Main.comptDict.containsKey(numUnique)){
                System.out.println("Ce compte n'existe pas dans le repertoire");
                principalCompte();
                return;
            }

            Utilisateur.TypeCompte tc = Main.comptDict.get(numUnique).getTypeCompte();
            infoCompte(tc,numUnique);
        }
    };

    /**
     * Permet de consulter tous les comptes du type specifie
     * @param tc type de compte
     */
    public void consulterListeCompte(Utilisateur.TypeCompte tc){
        for(Map.Entry<String, Utilisateur> entry : Main.comptDict.entrySet()){
            if(entry.getValue().getTypeCompte().equals(tc)){
                System.out.println(entry.getValue().toString());
            }
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez le numéro unique d'un compte que vous désirez consulter ou q pour quitter");
        String option = scanner.nextLine();

        if(option.equalsIgnoreCase("q")){
            menu.afficherMenuPrincipal();

        } else if(!Main.comptDict.containsKey(option)){
            System.out.println("L'entrée n'est pas valide" );
            consulterListeCompte(tc);

        }else{
            infoCompte(tc,option);
        }

    }

    /**
     * Affiche les informations d'un compte a patir de son numero unique
     * @param tc type de compte
     * @param numUnique numero d'identification unique propre au compte
     */
    public void infoCompte(Utilisateur.TypeCompte tc, String numUnique){
        if(tc == Utilisateur.TypeCompte.VISITEUR){
            if(Main.comptDict.containsKey(numUnique) &&
                    Main.comptDict.get(numUnique).getTypeCompte() == Utilisateur.TypeCompte.VISITEUR){
                Visiteur visiSupp = (Visiteur) Main.comptDict.get(numUnique);
                System.out.println(visiSupp.showInfo());
            }
        }

        if(tc == Utilisateur.TypeCompte.EMPLOYE){
            if(Main.comptDict.containsKey(numUnique) &&
                    Main.comptDict.get(numUnique).getTypeCompte()== Utilisateur.TypeCompte.EMPLOYE){
                Employe emplSupp = (Employe) Main.comptDict.get(numUnique);
                System.out.println(emplSupp.showInfo());
            }
        }

        if(tc == Utilisateur.TypeCompte.BENEVOLE){
            if(Main.comptDict.containsKey(numUnique) &&
                    Main.comptDict.get(numUnique).getTypeCompte() == Utilisateur.TypeCompte.BENEVOLE){
                Benevole beneSupp = (Benevole) Main.comptDict.get(numUnique);
                System.out.println(beneSupp.showInfo());
            }
        }
    }
}
