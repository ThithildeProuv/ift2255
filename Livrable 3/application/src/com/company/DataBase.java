package com.company;

import com.sun.jdi.BooleanValue;

import java.security.Key;
import java.util.HashMap;
import java.io.*;
import java.util.Map;
import java.util.logging.FileHandler;

public class DataBase {
    /**
     * Classe qui sauvegarde les compte d'utilisateur dans un fichier
     * .txt
     */
    static String outputFilePath = "database.txt";

    /**
     * methode qui sauve la HashMap<String, Utilisateur> dans un ficher .txt
     * @param data la hashmap qui sert de base de donnée à l'application.
     */
    public void save(HashMap<String, Utilisateur> data) {


        File file = new File(outputFilePath);
        BufferedWriter bfw = null;

        try {
            bfw = new BufferedWriter(new FileWriter(file));

            for (Map.Entry<String, Utilisateur> entry : data.entrySet()) {
                bfw.write(entry.getKey() + ";" + entry.getValue().storeInfo());
                bfw.newLine();
            }

            bfw.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {

                bfw.close();

            }
            catch (Exception e) {
            }
        }
    }
    /**
     * Load()  construit la
     * @return intialiseDataBase retrurne un hashmap qui sert de base de donnée à l'application.
     */

    public HashMap<String, Utilisateur> load(GenerateurId genId){
        HashMap<String,Utilisateur> initailisedDataBase = new HashMap<String, Utilisateur>();
        HashMap<String, String> rawDataBase = new HashMap<String, String>();

        BufferedReader bfr = null;


        try{
            File file = new File(outputFilePath);



            try{
                if(file.createNewFile()){
                   return initailisedDataBase;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            bfr = new BufferedReader(new FileReader((file)));

            String line = null;
            // L'idée est de divisé la le string spécifique et de recréer les objects
            // dans un autre contexte la mise en place d'un serveur serait plus approprié peut-être
            while ((line = bfr.readLine()) !=null){
                String[] parts = line.split(";");

                String numeroUnique = parts[0].trim();
                String userInfo = parts[1].trim();

                if(!numeroUnique.equals("") && !userInfo.equals("")){
                    rawDataBase.put(numeroUnique, userInfo);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        for(Map.Entry<String, String> entry : rawDataBase.entrySet()){
            String[] user = entry.getValue().split("=");
            //for(int i=0 ; i<user.length; i++){
            //System.out.println(user[i]);
            //}

            String nom = user[1].trim();
            String prenom = user[2].trim();
            String courriel = user[3].trim();
            String numTelephone = user[4].trim();

            if (user[0].trim().equals("Visiteur")){
                String dob = user[5].trim();
                Visiteur visiteur = new Visiteur(nom,prenom,courriel, numTelephone,dob,entry.getKey());
                genId.addIdUtilisateur(entry.getKey());

                if(!user[6].equals("none")){
                    String[] profilVac = user[6].split("&");

                    for(int i = 0; i<profilVac.length-1;i++){

                        String[] infoVac = profilVac[i].split(",");

                        String dateVac = infoVac[1].trim();
                        String typeDose = infoVac[2].trim();
                        String nomVac = infoVac[3].trim();
                        String codeVac = infoVac[4].trim();

                        visiteur.getProfilsVaccinaux().add(
                                new ProfilVaccination(visiteur.getNumUnique(),dateVac, Integer.valueOf(typeDose),nomVac,codeVac));
                    }
                }

                if (!user[7].equals("none")){
                    String[] rendezVous = user[7].split("&");

                    for(int i = 0; i<rendezVous.length-1; i++){
                        String[] infoRV = rendezVous[i].split(",");

                        String numRV = infoRV[0].trim();
                        String typeDose = infoRV[1].trim();
                        boolean confirmer = Boolean.parseBoolean(infoRV[3].trim());
                    }
                }
                initailisedDataBase.put(entry.getKey(),visiteur);
            }

            if(user[0].equals("Benevole")){
                String mdp = user[5].trim();
                String address = user[6].trim();
                String cp = user[7].trim();
                String ville = user[8].trim();
                String dispo = user[9].trim();
                String indentification = user[10].trim();

                Benevole benevole = new Benevole(nom, prenom,courriel,numTelephone,mdp,address,cp,ville,dispo,entry.getKey(),indentification);
                genId.addCodeUtilisateur(indentification);
                genId.addIdUtilisateur(entry.getKey());
                initailisedDataBase.put(entry.getKey(), benevole);

            }

        }

        return initailisedDataBase;
    }
}

