package com.company;

import java.util.Scanner;

public class InterfaceReservations {
    /**
     * Menu pour effecture/modifier/supprimer des reservations.
     */
    private Employe employeCourant;
    private InterfaceMenu menu;
    private com.company.Formulaire formulaire = com.company.Formulaire.getInstance();

    /**
     * Constructeur
     * @param employe Employe connecte
     * @param menu pour retourner au menu principal
     */
    InterfaceReservations(Employe employe, InterfaceMenu menu){
        this.employeCourant = employe;
        this.menu = menu;
    }

    /**
     * Menu principal des reservations: choisir de creer, confirmer, supprimer ou modifier un rendez-vous, ou
     * retourner au menu principal.
     */
    public void principalReservation(){
        System.out.println("   ---   Menu Reservation   ---   ");
        Scanner scanner = new Scanner(System.in);
        System.out.println("1: Prendre un rendez vous");
        System.out.println("2: Confirmer un rendez vous");
        System.out.println("3: Modifier/Annuler un rendez vous");
        System.out.println("4: Retour au menu principal");
        String choix = scanner.nextLine();

        switch (choix){
            case "1" -> prendreRendezvous();
            case "2" -> confirmerRendezvous();
            case "3" -> supprimerRendezVous();
            case "4" -> menu.afficherMenuPrincipal();
        }
    }

    /**
     * Permet de creer un objet rendez-vous a partir des inputs de l'utilisateur.
     */
    private void prendreRendezvous() {
        System.out.println("Choisir une plage horaire :");
        System.out.println(Main.calendrier.showNbPlaces());
        Scanner scanner = new Scanner(System.in);
        PlageHoraire plageHoraire = null;
        String horaire;
        do {
            System.out.println("Choisir un créneau (format AAAA-MM-JJTHH:MM:SS ex 2021-11-18T10:00)");
            horaire = scanner.nextLine();
            plageHoraire = Main.calendrier.getPlageHoraire(horaire);
            if (plageHoraire == null){
                System.out.println("Plage horaire non disponible");
            } else {
                break;
            }
        } while (!horaire.equals("exit"));

        int typeDose = formulaire.getTypeDoseFormulaire();
        String nom = formulaire.getInfoFormulaire("nom", 50);
        String prenom = formulaire.getInfoFormulaire("prénom", 50);
        String courriel = formulaire.getInfoFormulaire("courriel", 100);

        if (!nom.equals("exit") && !prenom.equals("exit") && !courriel.equals("exit")){
            RendezVous rendezVous = new RendezVous(typeDose,nom,prenom,courriel,plageHoraire);
            plageHoraire.addRendezVous(rendezVous);
            System.out.println("Le rendez-vous a été ajouté au calendrier.");
        }
    }

    /**
     * Permet de confirmer un rendez-vous a partir d'un numero de reservation.
     */
    private void confirmerRendezvous() {
        System.out.println(Main.calendrier.showRendezVous());
        System.out.println("Entrez le numéro de la réservation à confirmer");
        String numeroRendezVous = formulaire.getNumeroFormulaire("réservation", 6);
        System.out.println("Entrez le numéro unique du titulaire du rendez-vous");
        String numUnique = formulaire.getNumeroFormulaire("unique", 12);
        RendezVous rendezVous = Main.calendrier.getRendezVous(numeroRendezVous);
        if (rendezVous == null){
            System.out.println("Rendez-vous non existant");
        } else {
            rendezVous.confirmer();
            Visiteur visiteur = (Visiteur) Main.comptDict.get(numUnique);
            visiteur.ajouterVisite(Main.calendrier.getRendezVous(numeroRendezVous));
            System.out.println("Le rendez-vous " + rendezVous + " a été confirmé");
        }
    }

    /**
     * Permet de supprimer un rendez-vous a partir du numero de reservation.
     */
    private void supprimerRendezVous() {
        System.out.println(Main.calendrier.showRendezVous());
        System.out.println("Entrez le numéro de la réservation à supprimer");
        String numeroRendezVous = formulaire.getNumeroFormulaire("réservation", 6);
        RendezVous rendezVous = Main.calendrier.getRendezVous(numeroRendezVous);
        if (rendezVous == null){
            System.out.println("Rendez-vous non existant");
        } else {
            Main.calendrier.supprimerRendezVous(numeroRendezVous);
            System.out.println("Le rendez-vous " + rendezVous + " a été supprimé");
        }
    }
}
