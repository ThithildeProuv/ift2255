package com.company;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;

public class Calendrier {
    LinkedList<PlageHoraire> plageHoraires = new LinkedList<>();
    LocalDateTime dateDebut;
    int dureeEnJour;


    /**
     * Un Calendrier contient une liste de plage horaires
     * @param dateDebut le jour du debut du calendrier
     * @param dureeEnJour la durée du calendirer
     */
    Calendrier(LocalDate dateDebut, int dureeEnJour){
        LocalDateTime dateTimeDebut = LocalDateTime.parse(dateDebut.toString()+"T00:00:00");

        this.dateDebut = dateTimeDebut.minusHours(dateTimeDebut.getHour());
        this.dureeEnJour = dureeEnJour;

        for(long i=0; i<this.dureeEnJour; i++){
            LocalDateTime jour = dateTimeDebut.plusDays(i);
            if(jour.getDayOfWeek() != DayOfWeek.SATURDAY && jour.getDayOfWeek() != DayOfWeek.SUNDAY){
                for(long j=8; j<18; j++){
                    LocalDateTime debut = dateTimeDebut.plusDays(i).plusHours(j);
                    LocalDateTime fin = debut.plusHours(1);
                    PlageHoraire pH = new PlageHoraire(debut);
                    plageHoraires.add(pH);
                }
            }
        }
    }

    /**
     * représentation d'un calendrier avec ses plages horaires
     * @return
     */
    public String toString(){
        StringBuilder result = new StringBuilder();
        for (PlageHoraire pH : this.plageHoraires){
            result.append(pH.toString()).append("\n");
        }
        return result.toString();
    }

    /**
     * représentation d'un calendrier avec le nb de places disponible pour chaque plage horaire
     * @return
     */
    public String showNbPlaces(){
        StringBuilder result = new StringBuilder();
        for (PlageHoraire pH : this.plageHoraires){
            result.append(pH.showNbPlaces()).append("\n");
        }
        return result.toString();
    }

    /**
     * représentation d'un calendrier avec la liste des rendez-vous de chaque plage horaire
     * @return
     */
    public String showRendezVous(){
        StringBuilder result = new StringBuilder();
        for (PlageHoraire pH : this.plageHoraires){
            result.append(pH.showRendezVous()).append("\n");
        }
        return result.toString();
    }

    /**
     * cherche une plage horaire dans la liste des plages horaires
     * @param horaire string qui contient une heure
     * @return la plage horaire recherchée si on l'a, null sinon
     */
    public PlageHoraire getPlageHoraire(String horaire){
        for (PlageHoraire pH : plageHoraires){
            if (pH.isEquals(horaire)){
                return pH;
            }
        }
        return null;
    }

    /**
     * cherche un rendez-vous par son numéro
     * @param numeroRendezVous string du numéro du rendez-vous à rechercher
     * @return le rendez-vous recherché si on l'a, null sinon
     */
    public RendezVous getRendezVous(String numeroRendezVous){
        for (PlageHoraire pH : plageHoraires){
            if (pH.getRendezVous(numeroRendezVous) != null){
                return pH.getRendezVous(numeroRendezVous);
            }
        }
        return null;
    }

    /**
     * supprime un rendez-vous
     * @param numeroRendezVous string du numéro du rendez-vous à supprimer
     */
    public void supprimerRendezVous(String numeroRendezVous){
        for (PlageHoraire pH : plageHoraires){
            if (pH.getRendezVous(numeroRendezVous) != null){
                pH.supprimerRendezVous(numeroRendezVous);
            }
        }
    }
}
