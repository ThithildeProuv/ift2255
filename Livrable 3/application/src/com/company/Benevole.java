package com.company;

public class Benevole extends Personnel {
    private String disponibilite;

    public Benevole(String nom, String prenom, String courriel, String numTelephone,
                    String motDePasse, String adresse, String codePostal, String ville, String dispo){
        super(nom, prenom, courriel, numTelephone, TypeCompte.BENEVOLE, motDePasse, adresse,
                codePostal, ville);

        this.disponibilite = dispo;
    }

    public Benevole(String nom, String prenom, String courriel, String numTelephone, String motDePasse,
                    String adresse, String codePostal, String ville, String dispo, String id,String codeIdentification){
        super(nom, prenom, courriel, numTelephone, TypeCompte.BENEVOLE, motDePasse, adresse,
                codePostal, ville, id,codeIdentification);

        this.disponibilite = dispo;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public String showInfo(){
        return "|#Unique: "+getNumUnique()+"|\n|Nom: "+getNom()+"|\n|Prenom: "+getPrenom()+"|\n|Courriel: "+getCourriel()+
                "|\n|#Telephone: "+getNumTelephone()+"|\n|Mot de pass: "+getMotDePasse()+"|\n|Adresse: "+getAddress()+
                "|\n|Code postal: "+getCodePostal()+"|\n|Ville: "+getVille()+"|\n|Disponibilite: "+getDisponibilite()+"|";
    };


    /**
     * Formatte l'objet dans le format utilise par la BD
     * @return String formattee
     */

    public String getDisponibilite() {
        return disponibilite;
    }


    /**
     * Formattage des informations pour la consultation avec des espaces
     * @return String des informations
     */
    public String toString(){
        return "|#Unique: "+getNumUnique()+"| Nom: "+getNom()+"| Prenom: "+getPrenom()+"| Courriel: "+getCourriel()+
                "| #Telephone: "+getNumTelephone()+"| Mot de pass: "+getMotDePasse()+"| Adresse: "+getAddress()+
                "| Code postal: "+getCodePostal()+"| Ville: "+getVille()+"| Disponibilite: "+getDisponibilite()+"|";
    }

    /**
     * Formatte l'objet dans le format utilise par la BD
     * @return String formattee
     */
    public String storeInfo(){
        return "Benevole"+"="+getNom()+"="+getPrenom()+"="+getCourriel()+"="+getNumTelephone()+"="+getMotDePasse()+"="+getAddress()+
                "="+getCodePostal()+"="+getVille()+"="+getDisponibilite()+"="+getCodeIndification();
    }


    public String[] getAccountInfo() {
        String[] infos = {getNom(), getPrenom(), getCourriel(), getNumTelephone(),
               getMotDePasse(), getAddress(), getCodePostal(), getVille(), getDisponibilite()};
        return infos;
    }
}

