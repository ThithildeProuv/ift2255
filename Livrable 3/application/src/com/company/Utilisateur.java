package com.company;

public abstract class Utilisateur {
        private final String numUnique;
        private String nom;
        private String prenom;
        private String courriel;
        private String numTelephone;
        public TypeCompte typeCompte;

        /**
         * Pour identifier les types de compte de chaque sous-classe
         */
        public enum TypeCompte{
                EMPLOYE,
                BENEVOLE,
                VISITEUR
        }

        /**
         * Donnees communes aux vistieurs et aux membres du personnel
         * @param nom
         * @param prenom
         * @param courriel
         * @param numTelephone
         * @param typeCompte donne par les sous-classes
         */
        Utilisateur(String nom, String prenom, String courriel, String numTelephone, TypeCompte typeCompte){
                this.numUnique = Main.generateurId.generateIdUtilisateur();
                this.nom = nom;
                this.prenom = prenom;
                this.courriel = courriel;
                this.numTelephone = numTelephone;
                this.typeCompte = typeCompte;
        }

        /**
         * Constructeur utilise par la BD
         */
        Utilisateur(String nom, String prenom, String courriel, String numTelephone, TypeCompte typeCompte, String id){
                this.numUnique = id;
                this.nom = nom;
                this.prenom = prenom;
                this.courriel = courriel;
                this.numTelephone = numTelephone;
                this.typeCompte = typeCompte;
        }

        /**
         * Formatte l'objet dans le format utilise par la BD
         * @return String formattee
         */
        public abstract String storeInfo();

        /**
         * Formattage des informations pour la consultation
         * @return une string pour montrer les informations
         */
        public abstract String toString();

        /**
         * Pour les tests, retourne les informations liees au compte utilisateur sans l'identifiant unique
         * @return informations sans ID unique
         */
        public abstract String[] getAccountInfo();

        public String getNom() {
                return nom;
        }

        public void setNom(String nouveauNom) {
                this.nom = nouveauNom;
        }

        public String getPrenom() {
                return prenom;
        }

        public void setPrenom(String nouveauPrenom) {
                this.prenom = nouveauPrenom;
        }

        public String getCourriel() {
                return courriel;
        }

        public void setCourriel(String nouveauCourriel) {
                this.courriel = nouveauCourriel;
        }

        public String getNumTelephone() {
                return numTelephone;
        }

        public void setTelephone(String nouveauNumTelephone) {
                this.numTelephone = numTelephone;
        }

        public String getNumUnique() {
                return numUnique;
        }

        public TypeCompte getTypeCompte() {
                return typeCompte;
        }

        public void setTypeCompte(TypeCompte typeCompte) {
                this.typeCompte = typeCompte;
        }
}
