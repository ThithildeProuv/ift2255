package com.company;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;

public class Calendrier {
    LinkedList<PlageHoraire> plageHoraires = new LinkedList<>();
    LocalDateTime dateDebut;
    int dureeEnJour;


    Calendrier(LocalDate dateDebut, int dureeEnJour){
        LocalDateTime dateTimeDebut = LocalDateTime.parse(dateDebut.toString()+"T00:00:00");

        this.dateDebut = dateTimeDebut.minusHours(dateTimeDebut.getHour());
        this.dureeEnJour = dureeEnJour;

        for(long i=0; i<this.dureeEnJour; i++){
            LocalDateTime jour = dateTimeDebut.plusDays(i);
            if(jour.getDayOfWeek() != DayOfWeek.SATURDAY && jour.getDayOfWeek() != DayOfWeek.SUNDAY){
                for(long j=8; j<18; j++){
                    LocalDateTime debut = dateTimeDebut.plusDays(i).plusHours(j);
                    LocalDateTime fin = debut.plusHours(1);
                    PlageHoraire pH = new PlageHoraire(debut, fin);
                    plageHoraires.add(pH);
                }
            }
        }
    }

    public String showNbPlaces(){
        String result = "";
        for (PlageHoraire pH : this.plageHoraires){
            result += pH.showNbPlaces() + "\n";
        }
        return result;
    }
    public String showRendezVous(){
        String result = "";
        for (PlageHoraire pH : this.plageHoraires){
            result += pH.showRendezVous() + "\n";
        }
        return result;
    }

    public String toString(){
        String result = "";
        for (PlageHoraire pH : this.plageHoraires){
            result += pH.toString() + "\n";
        }
        return result;
    }

    public PlageHoraire getPlageHoraire(String horaire){
        for (PlageHoraire pH : plageHoraires){
            if (pH.equals(horaire)){
                return pH;
            }
        }
        return null;
    }

    public RendezVous getRendezVous(String numeroRendezVous){
        for (PlageHoraire pH : plageHoraires){
            if (pH.getRendezVous(numeroRendezVous) != null){
                return pH.getRendezVous(numeroRendezVous);
            }
        }
        return null;
    }

    public void supprimerRendezVous(String numeroRendezVous){
        for (PlageHoraire pH : plageHoraires){
            if (pH.getRendezVous(numeroRendezVous) != null){
                pH.supprimerRendezVous(numeroRendezVous);
            }
        }
    }
}
