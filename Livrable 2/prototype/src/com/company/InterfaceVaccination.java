package com.company;

import java.util.*;

public class InterfaceVaccination {
    private Employe employeCourant;
    private InterfaceMenu menu;
    private com.company.Formulaire formulaire = com.company.Formulaire.getInstance();

    InterfaceVaccination(Employe employe, InterfaceMenu menu){

    }

    public void principaleVaccin(){
        System.out.println("   ---   Menu Vaccination   ---   ");
        Scanner scanner = new Scanner(System.in);
        System.out.println("1: Questionnaire");
        System.out.println("2: Profil Vaccinal");
        System.out.println("3: Retour au menu principal");
        String choix = scanner.nextLine();

        switch (choix){
            case "1" -> entrerQuestionnaire();
            case "2" -> entrerProfilVaccinal();
            case "3" -> {
                menu.afficherMenuPrincipal();
            }
            default -> {
                System.out.println("Ceci n'est pas une option");
                principaleVaccin();
            }
        }
    }

    private void entrerProfilVaccinal() {
        System.out.println("   ---   Création du profil de vaccination   ---   ");

        String numCompte = formulaire.getNumeroFormulaire("compte", 12);

        if (!Main.comptDict.containsKey(numCompte)) {
            System.out.println("Le numero de compte n'existe pas");
            principaleVaccin();
        }

        String dateVacc = formulaire.getDateFormulaire("vaccination");

        int typeDose = formulaire.getTypeDoseFormulaire();

        String nomVacc = formulaire.getNomVaccinFormulaire();

        String codeVacc = formulaire.getCodeVaccinFormulaire();

        ProfilVaccination newProVacc = new ProfilVaccination(numCompte, dateVacc, typeDose,
                nomVacc,codeVacc);
        newProVacc.envoyerRapport((Visiteur) Main.comptDict.get(newProVacc.getNumeroCompte()));
    }

    private void entrerQuestionnaire() {
        System.out.println("   ---   *Questionnaire*   ---   ");
        String numCompte = formulaire.getNumeroFormulaire("compte", 12);
        if (numCompte.equals("exit")) {
            return;
        }
        String nom  = formulaire.getInfoFormulaire("nom", 50);
        if (nom.equals("exit")) {
            return;
        }
        String prenom = formulaire.getInfoFormulaire("prenom", 50);
        if (prenom.equals("exit")) {
            return;
        }
        String ddn = formulaire.getDateFormulaire("naissance");
        if (ddn.equals("exit")) {
            return;
        }
        String nam = formulaire.getNumCharFormulaire("numero d'assurance maladie", 12);
        if (nam.equals("exit")) {
            return;
        }
        String dv = formulaire.getDateFormulaire("visite");
        if (dv.equals("exit")) {
            return;
        }

        String premiereDose = formulaire.getQuestionsOuiNon("Avez-vous deja recu une premiere dose? (oui/non)");
        String covid = formulaire.getQuestionsOuiNon("Avez-vous deja contracte la COVID? (oui/non)");
        String symptomes = formulaire.getQuestionsOuiNon("Avez-vous des symptomes de la COVID? (oui/non)");
        String allergies = formulaire.getQuestionsOuiNon("Avez-vous des allergies? (oui/non)");
        String vaccin = formulaire.getNomVaccinFormulaire();

        System.out.println( "Numero de compte: "+numCompte+"\n"+
                "Nom : "+nom+"\n"+
                "Prenom: "+prenom+"\n"+
                "Date de naissance: "+ddn+"\n"+
                "Numero d'assurance maladie: "+nam+"\n"+
                "Date de la visite: "+dv+"\n"+
                "Premiere dose?: "+premiereDose+"\n"+
                "Covid?: "+covid+"\n"+
                "Symptome?: "+symptomes+"\n"+
                "Allergie?: "+allergies+"\n"+
                "Vaccin desire: "+vaccin);

    }

    public void genererQuestionnaire(){}
    public void genererProfilVaccinal(){}
}
