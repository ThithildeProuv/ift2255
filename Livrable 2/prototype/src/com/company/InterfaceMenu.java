package com.company;

import java.util.Scanner;

public class InterfaceMenu {
    Employe currentEmploye = null;

    InterfaceMenu(Employe emp){
        this.currentEmploye = emp;
    }

    public void afficherMenuPrincipal(){
        java.lang.System.out.println("\n   ---  Menu  ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("1: Gestion des Rendez-Vous");
        java.lang.System.out.println("2: Gestion des profils vaccinaux");
        java.lang.System.out.println("3: Gestion des comptes");
        java.lang.System.out.println("q: Quitter");
        String choix = scanner.nextLine();

        switch (choix) {
            case "q" -> java.lang.System.exit(0);
            case "1" -> {
                InterfaceReservations gestionReservations = new InterfaceReservations(currentEmploye, this);
                gestionReservations.principalReservation();
            }
            case "2" -> {
                InterfaceVaccination gestionVaccin = new InterfaceVaccination(currentEmploye, this);
                gestionVaccin.principaleVaccin();
            }
            case "3" -> {
                InterfaceCompte gestionCompte = new InterfaceCompte(currentEmploye, this);
                gestionCompte.principalCompte();

            }
            default -> {
                java.lang.System.out.println("Commande non reconnue");
                afficherMenuPrincipal();
            }

        }
         afficherMenuPrincipal();
    };
}
