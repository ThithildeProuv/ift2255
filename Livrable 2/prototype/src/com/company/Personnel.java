package com.company;

public abstract class Personnel extends Utilisateur{
    static int identification = 0;
    private String codeIdentification;
    private String motDePasse;
    private String address;
    private String codePostal;
    private String ville;


    Personnel(String nom, String prenom, String courriel, String numTelephone, TypeCompte typeCompte,
              String mdp, String add, String cp, String ville){
        super(nom, prenom, courriel, numTelephone, typeCompte);

        this.motDePasse = mdp;
        this.address = add;
        this.codePostal = cp;
        this.ville = ville;

        codeIdentification = ""+identification;
        while(codeIdentification.length() != 9){
            this.codeIdentification = "0"+codeIdentification;
        }
        this.identification++;
    };

    public String showInfo(){
        return "|#Unique: "+getNumUnique()+"|\n|Nom: "+getNom()+"|\n|Prenom: "+getPrenom()+"|\n|Courriel: "+getCourriel()+
                "|\n|#Telephone: "+getNumTelephone()+"|\n|Mot de pass: "+getMotDePasse()+"|\n|Adresse: "+getAddress()+
                "|\n|Code postal: "+getCodePostal()+"|\n|Ville: "+getVille()+"|";
    };

    public String getCodeIndification() {
        return codeIdentification;
    }

    public void setCodeIndification(String codeIndification) {
        this.codeIdentification = codeIndification;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
