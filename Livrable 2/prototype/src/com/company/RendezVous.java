package com.company;

public class RendezVous {
    static int rendu = 1;
    String numeroRendezVous;
    int typeDose;
    String nom;
    String prenom;
    String courriel;
    PlageHoraire plageHoraire;
    boolean isConfirme = false;

    RendezVous (int typeDose, String nom, String prenom, String courriel, PlageHoraire plageHoraire){
        this.typeDose = typeDose;
        this.nom = nom;
        this.prenom = prenom;
        this.courriel = courriel;
        this.plageHoraire = plageHoraire;

        this.numeroRendezVous = ""+rendu;
        while(numeroRendezVous.length() != 6){
            this.numeroRendezVous = "0"+numeroRendezVous;
        }
        rendu++;
    }

    public String toString(){
        return numeroRendezVous + " de " + nom + " " + prenom + " le " + plageHoraire.toString();
    }

    public boolean equals(String numeroRendezVous){
        return this.numeroRendezVous.equals(numeroRendezVous);
    }

    public void envoyerDetailsRendezVous(){ /* envoyer par courriel*/ }

    public void confirmer(){ this.isConfirme = true; }
}
