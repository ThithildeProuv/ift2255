package com.company;

public class Benevole extends Personnel{
    private String disponibilite;

    Benevole(String nom, String prenom, String courriel, String numTelephone,
             String motDePasse, String adresse, String codePostal, String ville, String dispo){
        super(nom, prenom, courriel, numTelephone, TypeCompte.BENEVOLE, motDePasse, adresse,
                codePostal, ville);

        this.disponibilite = dispo;
    }

    public String showInfo(){
        return "|#Unique: "+getNumUnique()+"|\n|Nom: "+getNom()+"|\n|Prenom: "+getPrenom()+"|\n|Courriel: "+getCourriel()+
                "|\n|#Telephone: "+getNumTelephone()+"|\n|Mot de pass: "+getMotDePasse()+"|\n|Adresse: "+getAddress()+
                "|\n|Code postal: "+getCodePostal()+"|\n|Ville: "+getVille()+"|\n|Disponibilite: "+getDisponibilite()+"|";
    };

    public String toString(){
        return ""+getNom()+" "+getPrenom()+" B";
    }

    public String getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }
}
