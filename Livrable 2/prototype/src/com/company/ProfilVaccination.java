package com.company;

// classe du profil vacinal.
// Rapport vaccinal?


public class ProfilVaccination {
    private String numeroCompte;
    private String dateVaccination;
    private int typeDose;
    private String nomVaccin;
    private String codeVaccin;

    public ProfilVaccination(String numeroCompte, String dateVaccination,
                             int typeDose, String nomVaccin, String codeVaccin) {
        this.numeroCompte = numeroCompte;
        this.dateVaccination = dateVaccination;
        this.typeDose = typeDose;
        this.nomVaccin = nomVaccin;
        this.codeVaccin = codeVaccin;
    }

    public void envoyerRapport(Visiteur visiteur){
        visiteur.ajouterProfilsVaccinaux(this);
        String courriel = visiteur.getCourriel();
        //envoyer un courriel
    }
    public String getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(String numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public String getDateVaccination() {
        return dateVaccination;
    }

    public void setDateVaccination(String dateVaccination) {
        this.dateVaccination = dateVaccination;
    }

    public int getTypeDose() {
        return typeDose;
    }

    public void setTypeDose(int typeDose) {
        this.typeDose = typeDose;
    }

    public String getNomVaccin() {
        return nomVaccin;
    }

    public void setNomVaccin(String nomVaccin) {
        this.nomVaccin = nomVaccin;
    }

    public String getCodeVaccin() {
        return codeVaccin;
    }

    public void setCodeVaccin(String codeVaccin) {
        this.codeVaccin = codeVaccin;
    }

}
