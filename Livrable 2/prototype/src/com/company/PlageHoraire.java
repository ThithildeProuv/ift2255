package com.company;

import java.time.LocalDateTime;
import java.util.LinkedList;

public class PlageHoraire {
    private LocalDateTime dateDebut;
    private LocalDateTime dateFin;
    private LinkedList<RendezVous> listeRendezVous = new LinkedList<>();
    private LinkedList<RendezVous> listeVisiteSpontanee = new LinkedList<>();
    private static final int nbVisiteursMax = 15;

    PlageHoraire( LocalDateTime dateDebut, LocalDateTime dateFin){
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public boolean valider72H(){
        return true;
    }

    public boolean validerMaxVisiteur(){
        return true;
    }

    public boolean validerMax2personne(){
        return true;
    }

    public boolean validerMax40personne(){
        return true;
    }

    public boolean validerDateHeure(){
        return true;
    }

    public String toString(){
        String result = this.dateDebut.toString();
        return result;
    }

    public String showRendezVous(){
        String result = this.dateDebut.toString() + " " + listeRendezVous.toString();
        return result;
    }

    public String showNbPlaces(){
        String result = this.dateDebut.toString();
        int nbPlaces = nbVisiteursMax - this.listeRendezVous.size() - this.listeVisiteSpontanee.size();
        return result + " " + Integer.toString(nbPlaces) + " places disponibles";
    }

    public boolean equals(String horaire){
        try{
            return LocalDateTime.parse(horaire).equals(this.dateDebut);
        } catch(Exception e) {
            return false;
        }

    }

    public RendezVous getRendezVous(String numeroRendezVous){
        for (RendezVous rdv : listeRendezVous){
            if (rdv.equals(numeroRendezVous)){
                return rdv;
            }
        }
        return null;
    }

    public void supprimerRendezVous(String numeroRendezVous){
        RendezVous rendezVous = this.getRendezVous(numeroRendezVous);
        this.listeRendezVous.remove(rendezVous);
    }

    public void addRendezVous(RendezVous rendezVous){
        this.listeRendezVous.add(rendezVous);
    }
}


