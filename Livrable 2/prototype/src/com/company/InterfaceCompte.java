package com.company;

import java.util.Map;
import java.util.Scanner;

public class InterfaceCompte {
    private Employe courant;
    private InterfaceMenu menu;
    private com.company.Formulaire formulaire = com.company.Formulaire.getInstance();

    InterfaceCompte(Employe employe, InterfaceMenu menu){
        this.courant = employe;
        this.menu = menu;
    }

    public void principalCompte(){
        java.lang.System.out.println("\n   ---  Menu Compte ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("1: Gestion Visiteur");
        java.lang.System.out.println("2: Gestion Employe");
        java.lang.System.out.println("3: Gestion Benevole");
        System.out.println("4: Retour au menu principal");
        String choixType = scanner.nextLine();
        
        Utilisateur.TypeCompte tc;
        switch (choixType){
            case "1" -> tc = Utilisateur.TypeCompte.VISITEUR;
            case "2" -> tc = Utilisateur.TypeCompte.EMPLOYE;
            case "3" -> tc = Utilisateur.TypeCompte.BENEVOLE;
            default -> tc = null;
        }

        if(choixType.equals("4")){menu.afficherMenuPrincipal();}

        if(tc == null){
            System.out.println("Erreur");
            principalCompte();
        }
        
        java.lang.System.out.println("1: Ajouter");
        java.lang.System.out.println("2: Modifier");
        java.lang.System.out.println("3: Supprimer");
        System.out.println("4: Consulter");
        String choixAction = scanner.nextLine();

        
        switch (choixAction){
            case "1" -> creerCompte(tc);
            case "2" -> modifierCompte(tc);
            case "3" -> supprimerCompte(tc);
            case "4" -> consulterCompte();
               
        }


    }

    public void creerCompte(Utilisateur.TypeCompte tc){
        System.out.println("\n   ---   Création d'un compte   ---   \n");

        String prenom = formulaire.getInfoFormulaire("prénom", 50);
        String nom = formulaire.getInfoFormulaire("nom", 50);
        String cellNumber = formulaire.getNumeroFormulaire("telephone", 10);
        String courriel = formulaire.getInfoFormulaire("courriel",100);

        if(tc == Utilisateur.TypeCompte.VISITEUR){
            String birthday = formulaire.getDateFormulaire("naissance");

            Visiteur nVisiteur = courant.creerCompteVisiteur(nom,prenom,courriel,cellNumber, birthday);
            Main.comptDict.put(nVisiteur.getNumUnique(),nVisiteur);
            System.out.println(nVisiteur.showInfo());
            System.out.println(Main.comptDict);
        }

        if(tc == Utilisateur.TypeCompte.EMPLOYE || tc == Utilisateur.TypeCompte.BENEVOLE){
            String mdp = formulaire.getMotDePasseFormulaire();
            String adresse = formulaire.getInfoFormulaire("adresse civile", 100);
            String codePostal = formulaire.getNumCharFormulaire("code postal", 6);

            String ville = formulaire.getInfoFormulaire("ville", 50);

            if(tc == Utilisateur.TypeCompte.EMPLOYE){
                Employe nEmploye = courant.creerCompteEmploye(nom,prenom,courriel,cellNumber,mdp,adresse,codePostal,ville);
                Main.comptDict.put(nEmploye.getNumUnique(), nEmploye);
                System.out.println("Votre numero de personnel est: "+nEmploye.getCodeIndification());
                System.out.println(nEmploye.showInfo());
                System.out.println(Main.comptDict);
            }

            if(tc == Utilisateur.TypeCompte.BENEVOLE){
                String dispo = formulaire.getDateFormulaire("disponibilites");
                Benevole nBenevole = courant.creerCompteBenevole(nom, prenom, courriel, cellNumber,mdp, adresse,codePostal,ville,dispo);
                Main.comptDict.put(nBenevole.getNumUnique(), nBenevole);
                System.out.println("Votre numero de personnel est: "+nBenevole.getCodeIndification());
                System.out.println(nBenevole.showInfo());
                System.out.println(Main.comptDict);
            }
        }



    }

    private void supprimerCompte(Utilisateur.TypeCompte tc) {
        System.out.println("\n   ---   Menu de Supression d'un compte   ---   ");
        String numUniqueSupp = formulaire.getNumeroFormulaire("compte", 12);

        if(!Main.comptDict.containsKey(numUniqueSupp)){
            System.out.println("Ce numero de compte n'existe pas");
            return;
        }
        if(tc != Main.comptDict.get(numUniqueSupp).getTypeCompte()){
            System.out.println("Ce numero de compte n'est pas un visiteur");
            return;
        }

        infoCompte(tc, numUniqueSupp);
        String valide = formulaire.getQuestionsOuiNon("Etes-vous certain.e de vouloir supprimer le compte ci-dessus (oui/non)");

        switch(valide){
            case "oui" -> {
                courant.supprimerCompte(numUniqueSupp);
                System.out.println("Le compte a ete supprimé avec succes");
            }
            case "non" -> principalCompte();
            default -> System.out.println("Erreur: "+valide+"n'est pas une entree valide");
        }

        System.out.println(Main.comptDict);
    }

    private void modifierCompte(Utilisateur.TypeCompte tc) {
        System.out.println("   ---   Menu de modification   ---   ");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrer le numero unique du compte a modifier");
        String numUniqueMod = formulaire.getNumeroFormulaire("compte", 12);

        if(!Main.comptDict.containsKey(numUniqueMod)){
            System.out.println("Ce compte n'existe pas dans le repertoire");
            principalCompte();
        }

        infoCompte(tc, numUniqueMod);

        System.out.println("Entrez le nom de l'information que vous voulez modifier (important de respecter l'orthographe" +
                " exacte");
        String info = scanner.nextLine();
        System.out.println("Entrez la nouvelle information en respectant le formatage (Date : AAAA-MM-JJ)");
        String nouvelleInfo = formulaire.getDateFormulaire("modification");

        if(tc == Utilisateur.TypeCompte.VISITEUR){
            courant.modifierCompteVisi((Visiteur) Main.comptDict.get(numUniqueMod), info, nouvelleInfo);
            System.out.println(((Visiteur) Main.comptDict.get(numUniqueMod)).showInfo());
        }

        if(tc == Utilisateur.TypeCompte.EMPLOYE){
            courant.modifierCompteEmpl((Employe) Main.comptDict.get(numUniqueMod), info, nouvelleInfo);
            System.out.println(((Employe) Main.comptDict.get(numUniqueMod)).showInfo());
        }

        if(tc == Utilisateur.TypeCompte.BENEVOLE){
            courant.modifierCompteBene((Benevole) Main.comptDict.get(numUniqueMod), info, nouvelleInfo);
            System.out.println(((Benevole) Main.comptDict.get(numUniqueMod)).showInfo());
        }

        System.out.println("Modification effectuee avec succes");

    }

    public void consulterCompte(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Rechercher par...(Courriel et Date/#Unique)");
        String recherche = scanner.nextLine();
        if(recherche.equals("Courriel et Date")){
            String courriel = formulaire.getInfoFormulaire("courriel", 100);
            String ddn = formulaire.getDateFormulaire("naissance");

            for (Map.Entry<String,Utilisateur> entry : Main.comptDict.entrySet()){
                Utilisateur utilisateur = entry.getValue();
                if(utilisateur.getTypeCompte() == Utilisateur.TypeCompte.VISITEUR){
                    Visiteur visiteur = (Visiteur) utilisateur;
                    if(visiteur.getDateNaissance().equals(ddn) && visiteur.getCourriel().equals(courriel)){
                        infoCompte(visiteur.getTypeCompte(), visiteur.getNumUnique());
                    }
                }
            }
        }
        if(recherche.equals("#Unique")){
            String numUnique  = formulaire.getNumeroFormulaire("compte", 12);

            if(!Main.comptDict.containsKey(numUnique)){
                System.out.println("Ce compte n'existe pas dans le repertoire");
                principalCompte();
            }

            Utilisateur.TypeCompte tc = Main.comptDict.get(numUnique).getTypeCompte();
            infoCompte(tc,numUnique);
        }

    };

    public void infoCompte(Utilisateur.TypeCompte tc, String numUnique){
        if(tc == Utilisateur.TypeCompte.VISITEUR){
            if(Main.comptDict.containsKey(numUnique) &&
                    Main.comptDict.get(numUnique).getTypeCompte() == Utilisateur.TypeCompte.VISITEUR){
                Visiteur visiSupp = (Visiteur) Main.comptDict.get(numUnique);
                System.out.println(visiSupp.showInfo());
            }
        }

        if(tc == Utilisateur.TypeCompte.EMPLOYE){
            if(Main.comptDict.containsKey(numUnique) &&
                    Main.comptDict.get(numUnique).getTypeCompte()== Utilisateur.TypeCompte.EMPLOYE){
                Employe emplSupp = (Employe) Main.comptDict.get(numUnique);
                System.out.println(emplSupp.showInfo());
            }
        }

        if(tc == Utilisateur.TypeCompte.BENEVOLE){
            if(Main.comptDict.containsKey(numUnique) &&
                    Main.comptDict.get(numUnique).getTypeCompte() == Utilisateur.TypeCompte.BENEVOLE){
                Benevole beneSupp = (Benevole) Main.comptDict.get(numUnique);
                System.out.println(beneSupp.showInfo());
            }
        }
    }
}
