package com.company;

import java.util.LinkedList;

/*
* Représente les visiteurs, qui sont des utilisateurs du système avec une date de naissance et un choix de vaccin.
* */
public class Visiteur extends Utilisateur{
    private String dateNaissance;
    private LinkedList<RendezVous> visites;
    private LinkedList<ProfilVaccination> profilsVaccinaux = new LinkedList<ProfilVaccination>();


    Visiteur(String nom, String prenom, String courriel, String numTelephone,
           String dateNaissance){
        super(nom, prenom, courriel, numTelephone, TypeCompte.VISITEUR);
        this.dateNaissance = dateNaissance;
    }

    public String toString(){
      return ""+getNom()+" "+getPrenom()+" V";
    };

    public String showInfo(){
        return "|#Unique: "+getNumUnique()+"|\n|Nom: "+getNom()+"|\n|Prenom: "+getPrenom()+"|\n|Courriel: "+getCourriel()+
                "|\n|#Telephone: "+getNumTelephone()+"|\n|Date de naissance: "+getDateNaissance()+
                "|\n|Profils vaccinaux: "+getProfilsVaccinaux().size();
    };

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String nouvelleDateNaissance) {
        this.dateNaissance = nouvelleDateNaissance;
    }

    public LinkedList<RendezVous> getVisites() {
        return visites;
    }

    // Ajoute une visite suite a un rendez-vous de vaccination
    public LinkedList<RendezVous> ajouterVisite(RendezVous visite) {
        visites.add(visite);
        return visites;
    }

    // Nombre de visites ne devrait pas depasser 2 (2 doses max pour le vaccin)
    public boolean validerMaxVisites() {
        return (visites.size() < 2);
    }

    public LinkedList<ProfilVaccination> getProfilsVaccinaux() {
        return profilsVaccinaux;
    }

    // Ajoute un profil vaccinal suite a un rendez-vous de vaccination
    public void ajouterProfilsVaccinaux(ProfilVaccination profilVaccinal) {
        profilsVaccinaux.add(profilVaccinal);
    }
}
