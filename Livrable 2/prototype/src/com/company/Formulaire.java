package com.company;

import java.math.BigInteger;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.regex.Pattern;

public final class Formulaire {
    /**
     * La classe formulaire fournit des méthodes liées à l'entrée d'information par les utilisateurs
     * aux interfaces (menu, vaccination, réservation, etc.) pour faciliter l'entrée de données valides.
     * La classe est un singleton et contient son scanner.
     */
    private static Formulaire formulaire = null;
    public static Scanner scanner;

    private Formulaire()
    {
        scanner = new Scanner(java.lang.System.in);
    }

    /*
    * Instantiation unique de la classe.
    */
    public static Formulaire getInstance()
    {
        if (formulaire == null)
            formulaire= new Formulaire();

        return formulaire;
    }

    /**
     * Valide les entrees numeriques de longueur exacte numLength.
     * @param typeNumero Indication pour l'utilisateur (Entrer votre numero de {typeNumero})
     * @param numLength Longueur exacte de l'entree
     * @return Numero valide ou exit dans le cas ou l'utilisateur veut quitter sans entrer de valeur.
     */
    public static String getNumeroFormulaire(String typeNumero, int numLength) {
        String numero = "";
        do {
            System.out.println(String.format("Entrer votre numero de %s.", typeNumero));
            numero = scanner.nextLine().replaceAll("\\s+", "");
            try {
                BigInteger num = new BigInteger(numero);
            } catch (Exception ex) {
                System.out.println(String.format("Le numéro de %s doit être composé de  %d chiffres.",
                        typeNumero, numLength));
                continue;
            }
            if (numero.length() != numLength) {
                System.out.println(String.format("Le numéro de %s doit être composé de %d chiffres.",
                        typeNumero, numLength));
            } else {
                return numero;
            }
        } while (!numero.equals("exit"));
        return "exit";
    }

    /**
     * Valide les entrees du type de dose.
     * @return 1 ou 2, selon l'entree de l'utilisateur.
     */
    public static int getTypeDoseFormulaire() {
        int typeDose = 0;
        while(typeDose != 1 && typeDose != 2) {
            System.out.println("Entrer type de dose (1 ou 2)");
            try{
                typeDose = Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                typeDose = 0;
            }
        }
        return typeDose;
    }

    /**
     * Offre a l'utilisateur un choix de vaccin : il entre un nombre selon ce qu'il veut.
     * @return le nom du vaccin choisi.
     */
    public static String getNomVaccinFormulaire(){
        Dictionary vaccins = new Hashtable();
        vaccins.put(1, "Moderna");
        vaccins.put(2, "Pfizer");
        vaccins.put(3, "AstraZenenca");
        vaccins.put(4, "Janssen");

        int numVaccin = 0;
        while(numVaccin == 0) {
            System.out.println("Choisir vaccin: \n" +
                    "1- Moderna \n" +
                    "2- Pfizer \n" +
                    "3- AstraZeneca \n" +
                    "4- Janssen \n");
            try{
                numVaccin = Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                numVaccin = 0;
            }
        }
        return (String) vaccins.get(numVaccin);
    }

    /**
     * Valide l'entree de l'utilisateur pour le code du vaccin (entre 1 et 24 caracteres).
     * @return le code vaccin entre sous forme valide ou exit si l'utilisateur desire quitter.
     */
    public static String getCodeVaccinFormulaire(){
        String codeVacc;
        do {
            System.out.println("Entrer le code du vaccin");
            codeVacc = scanner.nextLine().replaceAll("\\s+", "");
            if (codeVacc == "" || codeVacc.length() > 24){
                System.out.println("Entrée invalide");
            } else {
                return codeVacc;
            }
        } while(!codeVacc.equals("exit"));
        return "exit";
    }

    /**
     * Valide les entrees de date (AAAA-MM-JJ).
     * @param dateDemandee Indique a l'utilisateur quelle date est demandee
     *                     (e.g. Entrer l'annee/le mois/le jour de {dateDemandee}".
     * @return la date sous le format AAAA-MM-JJ ou exit si l'utilisateur desire quitter.
     */
    public static String getDateFormulaire(String dateDemandee){
        String annee, mois, jour;
        do {
            System.out.println(String.format("Entrer l'annee de %s (AAAA)", dateDemandee));
            annee = scanner.nextLine().replaceAll("\\s+", "");

            System.out.println(String.format("Entrer le mois de %s (MM)", dateDemandee));
            mois = scanner.nextLine().replaceAll("\\s+", "");

            System.out.println(String.format("Entrer le jour de %s (JJ)", dateDemandee));
            jour = scanner.nextLine().replaceAll("\\s+", "");

            if (annee.length() != 4 || mois.length() != 2 || jour.length() != 2){
                System.out.println("Entrée invalide");
            } else {
                return annee+"-"+mois+"-"+jour;
            }
        } while(!annee.equals("exit") && !mois.equals("exit") && !jour.equals("exit"));

        return "exit";
    }

    /**
     * Valide les informations alphanumeriques entrees par l'utilisateur
     * @param typeNom Type d'informations demandees (e.g. Votre {typeNom}?)
     * @param maxChar Nombre de caracteres maximum pouvant etre entres par l'utilisateur
     * @return l'information entree, entre 1 et maxChar caracteres ou exit si l'utilisateur desire quitter.
     */
    public static String getInfoFormulaire(String typeNom, int maxChar){
        String nom;
        do {
            System.out.println(String.format("Votre %s ?", typeNom));
            nom = scanner.nextLine().trim();
            if (nom == "" || nom.length() > maxChar){
                System.out.println("Entrée invalide");
            } else {
                return nom;
            }
        } while(!nom.equals("exit"));
        return "exit";
    }

    /**
     * Valide les entrees alphanumeriques d'une longueur exacte (ex. code postal ou numero d'assurance maladie)
     * @param dataType Type de donnees (Entrer votre {dataType})
     * @param dataLength Longeur exacte de l'entree demandee
     * @return l'entree validee ou exit si l'utilisateur desire quitter.
     */
    public static String getNumCharFormulaire(String dataType, int dataLength){
        String numChar;
        do {
            System.out.println(String.format("Entrer votre %s.", dataType));
            numChar = scanner.nextLine().replaceAll("\\s+", "");
            if (numChar.length() != dataLength){
                System.out.println("Entrée invalide");
            } else {
                return numChar;
            }
        } while(!numChar.equals("exit"));
        return "exit";
    }

    /**
     * Valide l'input pour des questions oui/non.
     * @param question Question complete a poser.
     * @return Reponse (oui/non)
     */
    public static String getQuestionsOuiNon(String question){
        String reponse;
        do {
            System.out.println(question);
            reponse = scanner.nextLine();

            if(reponse.equalsIgnoreCase("oui") || reponse.equalsIgnoreCase("non")){
                return reponse;
            }else {
                System.out.println("Entree invalide.");
            }
        } while(true);
    }

    /**
     * Valide le mot de passe choisi par l'utilisateur: une majuscule, une minuscule, un chiffre et un caractere special
     * @return le mot de passe valide ou exit si l'utilisateur desire quitter.
     */
    public static String getMotDePasseFormulaire(){
        String mdp;
        do {
            System.out.println("Entrer un mot de passe.");
            mdp = scanner.nextLine();

            boolean numMatcher = Pattern.compile("\\d").matcher(mdp).find();
            boolean capsMatcher = Pattern.compile("[A-Z]").matcher(mdp).find();
            boolean lowMatcher = Pattern.compile("[a-z]").matcher(mdp).find();
            boolean specMatcher = Pattern.compile("\\W").matcher(mdp).find();

            if(mdp.length() >= 8 && numMatcher && lowMatcher && capsMatcher && specMatcher){
                return mdp;
            }else {
                System.out.println("Mot de passe invalide. Doit etre composé d'au moins " +
                        "8 caractères contenant au moins 1 chiffre, 1 majuscule, 1 minuscule " +
                        "et 1 caractère spécial.");
            }
        } while(!mdp.equals("exit"));
        return "exit";
    }
}
