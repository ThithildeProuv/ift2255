package com.company;

/*
* La classe Employé conserve les informations de l'utilisateur du système, en plus d'informations personnelles.
* */
public class Employe extends Personnel{


    Employe(String nom, String prenom, String courriel, String numTelephone,
           String motDePasse, String adresse, String codePostal, String ville){
        super(nom, prenom, courriel, numTelephone, TypeCompte.EMPLOYE, motDePasse, adresse,
                codePostal, ville);
    }

    public Visiteur creerCompteVisiteur(String nom, String prenom, String courriel, String numTelephone,
                                        String dateNaissance){
        Visiteur nouveauVisiteur = new Visiteur(nom, prenom, courriel, numTelephone,dateNaissance);

        return nouveauVisiteur;
    }

    public Employe creerCompteEmploye(String nom, String prenom, String courriel, String numTelephone,
                                      String motDePasse, String adresse, String codePostal, String ville) {
        Employe nouveauEmploye = new Employe(nom, prenom, courriel, numTelephone, motDePasse, adresse, codePostal, ville);

        return nouveauEmploye;
    }

    public Benevole creerCompteBenevole(String nom, String prenom, String courriel, String numTelephone,
                                        String motDePasse, String adresse, String codePostal, String ville, String dispo){
        Benevole nouveauBenevole = new Benevole(nom, prenom, courriel, numTelephone, motDePasse, adresse, codePostal, ville, dispo);

        return  nouveauBenevole;
    }

    public void supprimerCompte(String numUnique){
        Main.comptDict.remove(numUnique);
    }

    public void modifierCompteVisi(Visiteur visiteur, String ags, String nouveau){
       switch (ags){
           case "Nom" -> visiteur.setNom(nouveau);
           case "Prenom" -> visiteur.setPrenom(nouveau);
           case "Courriel" -> visiteur.setCourriel(nouveau);
           case "#Telephone" -> visiteur.setTelephone(nouveau);
           case "Date de naissance" -> visiteur.setDateNaissance(nouveau);
           default -> System.out.println("La modification s'effectue un sur un argument qui n'existe pas...");
       }

    }

    public void modifierCompteEmpl( Employe employe, String ags, String nouveau){
        switch (ags){
            case "Nom" -> employe.setNom(nouveau);
            case "Prenom" -> employe.setPrenom(nouveau);
            case "Courriel" -> employe.setCourriel(nouveau);
            case "#Telephone" -> employe.setTelephone(nouveau);
            case "Mot de passe" -> employe.setMotDePasse(nouveau);
            case "Adresse" -> employe.setAddress(nouveau);
            case "Code postal" -> employe.setCodePostal(nouveau);
            case "Ville" -> employe.setVille(nouveau);
            default -> System.out.println("La modification s'effectue un sur un argument qui n'existe pas...");
        }
    }

    public void modifierCompteBene(Benevole benevole, String ags, String nouveau){
        switch (ags){
            case "Nom" -> benevole.setNom(nouveau);
            case "Prenom" -> benevole.setPrenom(nouveau);
            case "Courriel" -> benevole.setCourriel(nouveau);
            case "#Telephone" -> benevole.setTelephone(nouveau);
            case "Mot de passe" -> benevole.setMotDePasse(nouveau);
            case "Adresse" -> benevole.setAddress(nouveau);
            case "Code postal" -> benevole.setCodePostal(nouveau);
            case "Ville" -> benevole.setVille(nouveau);
            case "Disponibilite" -> benevole.setDisponibilite(nouveau);
            default -> System.out.println("La modification s'effectue un sur un argument qui n'existe pas...");
        }
    }


    public String toString(){
     return ""+getNom()+" "+getPrenom()+" E";
    }
}







