package com.company;

public abstract class
Utilisateur {
        static int numCompte = 1;
        private String numUnique;
        private String nom;
        private String prenom;
        private String courriel;
        private String numTelephone;
        private TypeCompte typeCompte;

        public enum TypeCompte{
                EMPLOYE,
                BENEVOLE,
                VISITEUR
        }

        Utilisateur(String nom, String prenom, String courriel, String numTelephone, TypeCompte typeCompte){
                this.nom = nom;
                this.prenom = prenom;
                this.courriel = courriel;
                this.numTelephone = numTelephone;
                this.numUnique = ""+numCompte;
                this.typeCompte = typeCompte;

                // Generer le numero unique à 12 chiffres -GP
                while(numUnique.length() != 12){
                        this.numUnique = "0"+numUnique;
                }
                numCompte++;
        }

        // Pourquoi on fait ca?
        // Je croyais sympa d'avoir une méthode qui affiche l'info d'un compte GP

        public String toString(){
                return ""+this.nom+" "+this.prenom;
        }

        public String getNom() {
                return nom;
        }

        public void setNom(String nouveauNom) {
                this.nom = nouveauNom;
        }

        public String getPrenom() {
                return prenom;
        }

        public void setPrenom(String nouveauPrenom) {
                this.prenom = nouveauPrenom;
        }

        public String getCourriel() {
                return courriel;
        }

        public void setCourriel(String nouveauCourriel) {
                this.courriel = nouveauCourriel;
        }

        public String getNumTelephone() {
                return numTelephone;
        }
        public void setTelephone(String nouveauNumTelephone) {
                this.numTelephone = numTelephone;
        }

        public String getNumUnique() {
                return numUnique;
        }

        public void setNumUnique(String numUnique) {
                this.numUnique = numUnique;
        }

        public TypeCompte getTypeCompte() {
                return typeCompte;
        }

        public void setTypeCompte(TypeCompte typeCompte) {
                this.typeCompte = typeCompte;
        }
}
