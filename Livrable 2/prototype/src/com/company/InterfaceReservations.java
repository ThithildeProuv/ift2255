package com.company;

import java.util.Scanner;

public class InterfaceReservations {
    private Employe employeCourant;
    private InterfaceMenu menu;
    private com.company.Formulaire formulaire = com.company.Formulaire.getInstance();

    InterfaceReservations(Employe employe, InterfaceMenu menu){
        this.employeCourant = employe;
        this.menu = menu;
    }

    public void principalReservation(){
        System.out.println("   ---   Menu Reservation   ---   ");
        Scanner scanner = new Scanner(System.in);
        System.out.println("1: Prendre un rendez vous");
        System.out.println("2: Confirmer un rendez vous");
        System.out.println("3: Modifier/Annuler un rendez vous");
        System.out.println("4: Retour au menu principal");
        String choix = scanner.nextLine();

        switch (choix){
            case "1" -> prendreRendezvous();
            case "2" -> confirmerRendezvous();
            case "3" -> supprimerRendezVous();
            case "4" -> menu.afficherMenuPrincipal();
        }
    }

    private void prendreRendezvous() {
        System.out.println("Choisir une plage horaire :");
        System.out.println(Main.calendrier.showNbPlaces());
        Scanner scanner = new Scanner(System.in);
        PlageHoraire plageHoraire = null;
        String horaire;
        do {
            System.out.println("Choisir un créneau (format AAAA-MM-JJTHH:MM:SS ex 2021-11-18T10:00)");
            horaire = scanner.nextLine();
            plageHoraire = Main.calendrier.getPlageHoraire(horaire);
            if (plageHoraire == null){
                System.out.println("Plage horaire non disponible");
            } else {
                break;
            }
        } while (!horaire.equals("exit"));

        int typeDose = formulaire.getTypeDoseFormulaire();
        String nom = formulaire.getInfoFormulaire("nom", 50);
        String prenom = formulaire.getInfoFormulaire("prénom", 50);
        String courriel = formulaire.getInfoFormulaire("courriel", 100);

        if (!nom.equals("exit") && !prenom.equals("exit") && !courriel.equals("exit")){
            RendezVous rendezVous = new RendezVous(typeDose,nom,prenom,courriel,plageHoraire);
            plageHoraire.addRendezVous(rendezVous);
            System.out.println("Le rendez-vous a été ajouté au calendrier.");
        }
    }

    private void confirmerRendezvous() {
        System.out.println(Main.calendrier.showRendezVous());
        System.out.println("Entrez le numéro de la réservation à confirmer");
        String numeroRendezVous = formulaire.getNumeroFormulaire("réservation", 6);
        RendezVous rendezVous = Main.calendrier.getRendezVous(numeroRendezVous);
        if (rendezVous == null){
            System.out.println("Rendez-vous non existant");
        } else {
            rendezVous.confirmer();
            System.out.println("Le rendez-vous " + rendezVous + " a été confirmé");
        }
    }

    private void supprimerRendezVous() {
        System.out.println(Main.calendrier.showRendezVous());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez le numéro de la réservation à supprimer");
        String numeroRendezVous = scanner.nextLine();
        RendezVous rendezVous = Main.calendrier.getRendezVous(numeroRendezVous);
        if (rendezVous == null){
            System.out.println("Rendez-vous non existant");
        } else {
            Main.calendrier.supprimerRendezVous(numeroRendezVous);
            System.out.println("Le rendez-vous " + rendezVous + " a été supprimé");
        }
    }
}
