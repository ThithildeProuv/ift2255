package com.company;

import java.time.LocalDate;
import java.util.HashMap;

public class Main {
    static HashMap<String, Utilisateur> comptDict = new HashMap<String, Utilisateur>();
    static Calendrier calendrier = new Calendrier(LocalDate.now(),7);


    public static void main(String[] args) {

        Employe currentEmploye = new Employe("Agere", "Ella","ellagere@gmail.com",
                "1234567", "Vaxtodo", "123 rue Despin",
                "1a1 a1a", "Montreal");

        InterfaceMenu menu = new InterfaceMenu(currentEmploye);
        menu.afficherMenuPrincipal();
    }




    /*public static void menu(){

        java.lang.System.out.println("\n   ---  Menu  ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("1: Prendre un rendez-vous");
        java.lang.System.out.println("2: Confirmer un rendez-vous (présence du visiteur)");
        java.lang.System.out.println("3: Créer un compte visiteur");
        java.lang.System.out.println("4: Créer un compte de personnel");
        java.lang.System.out.println("5: Mettre à jour un compte");
        java.lang.System.out.println("q: Quitter");
        String choix = scanner.nextLine();

        switch (choix) {
            case "q" -> java.lang.System.exit(0);
            case "1" -> prendreRdv();
            case "2" -> confirmerRdv();
            case "3" -> {
                Visiteur nv = CurrentEmploye.creerCompteVisiteur();
                comptDict.put(nv.getNumUnique(),nv);
                java.lang.System.out.println(comptDict);
                menu();
            }
            case "4" -> creerComptePersonnel();
            case "5" -> updateCompte();
            default -> {
                java.lang.System.out.println("Commande non reconnue");
                menu();
            }
        }
    }*/

    /*public static void prendreRdv(){
        java.lang.System.out.println("\n   ---   Prendre un rdv   ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("Accès au calendrier ...");
        java.lang.System.out.println("Choisir parmis les jours suivants:\t\t 1. jour 1\t\t 2. jour 2\t\t 3. jour 3");
        String jour = scanner.nextLine();
        java.lang.System.out.println("Choisir parmis les horaires suivants:\t 1. horaire 1\t 2. horaire 2\t 3. horaire 3");
        String heure = scanner.nextLine();
        //System.out.println("L'horaire est le " + jour + " à " + heure + "h");  // Output user input
        java.lang.System.out.println("Prénom ?");
        String prenom = scanner.nextLine();
        java.lang.System.out.println("Nom ?");
        String nom = scanner.nextLine();
        String name = prenom + " " + nom;
        //System.out.println("Vous êtes " + name);
        java.lang.System.out.println("Type de dose (1 ou 2) ?");
        String dose = scanner.nextLine();
        //System.out.println("C'est votre dose n°" + dose);
        java.lang.System.out.println("Adresse courriel ?");
        String courriel = scanner.nextLine();

        java.lang.System.out.println("\n... Chargement ...");
        java.lang.System.out.println("Résumé : rendez-vous pour " + name + " contactable par " + courriel + " pour sa dose n° " + dose + " le " + jour + " à " + heure + "h");
        java.lang.System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            prendreRdv();
        } else {
            Random random = new Random();
            String num = Integer.toString(random.nextInt(1000000000));
            java.lang.System.out.println("Le numéro unique de la réservation est " + num);
            java.lang.System.out.println("... Chargement ...");
            java.lang.System.out.println("Le rendez-vous est réservé. Retour au menu\n");
            menu.menuPrincipal();
        }
    }*/

    /*public static void confirmerRdv(){
        java.lang.System.out.println("\n   ---   Confirmation de rendez-vous   ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("Numéro de réservation ? (x pour inconnu)");
        String num = scanner.nextLine();
        String name = "Louis-Edouard Lafontant";
        if (num.equals("x")){
            java.lang.System.out.println("Prénom ?");
            String prenom = scanner.nextLine();
            java.lang.System.out.println("Nom ?");
            String nom = scanner.nextLine();
            name = prenom + " " + nom;
            Random random = new Random();
            num = Integer.toString(random.nextInt(1000000000));
        }
        //System.out.println("Vous êtes" + name);
        java.lang.System.out.println("Heure du rdv ?");
        String heure = scanner.nextLine();

        java.lang.System.out.println("\n... Chargement ...");
        java.lang.System.out.println("Résumé : rendez-vous " + num + " de " + name + " à " + heure + "h");
        java.lang.System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            confirmerRdv();
        } else {
            java.lang.System.out.println("... Chargement ...");
            java.lang.System.out.println("Le rendez-vous est confirmé. Retour au menu\n");
            menu();
        }
    }*/

    /*public static void creerCompteVisiteur(){
        java.lang.System.out.println("\n   ---   Création d'un compte de visiteur   ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("Prénom ?");
        String prenom = scanner.nextLine();
        java.lang.System.out.println("Nom ?");
        String nom = scanner.nextLine();
        String name = prenom + " " + nom;
        java.lang.System.out.println("Date de naissance (AAAA-MM-JJ) ?");
        String birthday = scanner.nextLine();
        java.lang.System.out.println("Numéro de téléphone ?");
        String cellNumber = scanner.nextLine();
        java.lang.System.out.println("Adresse courriel ?");
        String courriel = scanner.nextLine();
        java.lang.System.out.println("Numéros de réservation s'il y en a ? (laisser vide sinon)");
        String reservations = scanner.nextLine();

        java.lang.System.out.println("\n... Chargement ...");
        java.lang.System.out.println("Résumé : visiteur " + name + ", né le " + birthday + ", contactable au " + cellNumber + " ou par " + courriel);
        if (reservations.length()>0) {
            java.lang.System.out.println("Ses réservations sont : " + reservations);
        }
        java.lang.System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            creerCompteVisiteur();
        } else {
            Random random = new Random();
            String num = Integer.toString(random.nextInt(1000000000));
            java.lang.System.out.println("Le numéro unique du compte est " + num);
            java.lang.System.out.println("... Chargement ...");
            java.lang.System.out.println("Le compte du visiteur est créé. Retour au menu\n");
            menu();
        }
    }*/

    /*public static void creerComptePersonnel(){
        java.lang.System.out.println("\n   ---   Création d'un compte de personnel   ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("Prénom ?");
        String prenom = scanner.nextLine();
        java.lang.System.out.println("Nom ?");
        String nom = scanner.nextLine();
        String name = prenom + " " + nom;
        java.lang.System.out.println("Numéro de téléphone ?");
        String cellNumber = scanner.nextLine();
        java.lang.System.out.println("Adresse postale ?");
        String adresse = scanner.nextLine();
        java.lang.System.out.println("Adresse courriel ?");
        String courriel = scanner.nextLine();
        java.lang.System.out.println("employé ou bénévole ? (respecter l'orthographe, les accents et la casse)");
        String poste = scanner.nextLine();
        java.lang.System.out.println("Jours de disponibilité ? (séparés par des espaces)");
        String dispos = scanner.nextLine();

        java.lang.System.out.println("\n... Chargement ...");
        java.lang.System.out.println("Résumé : " + poste + " " + name + ", contactable au " + cellNumber + " ou par " + courriel);
        java.lang.System.out.println("Disponible les " + dispos);
        java.lang.System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            creerComptePersonnel();
        } else {
            Random random = new Random();
            String num = Integer.toString(random.nextInt(1000000000));
            java.lang.System.out.println("Le numéro unique du compte est " + num);
            if (poste.equals("employé")){
                String numEmploye = Integer.toString(random.nextInt(1000000));
                java.lang.System.out.println("Le code de l'employé est " + numEmploye);
                java.lang.System.out.println("Choix du mot de passe");
                String password = scanner.nextLine();
                boolean passwordValide = password.length() >= 8;
                while (!passwordValide){
                    java.lang.System.out.println("Mot de passe non valide");
                    java.lang.System.out.println("Choix du mot de passe (au moins 8 caractères)");
                    password = scanner.nextLine();
                    passwordValide = password.length() >= 8;
                }
                java.lang.System.out.println("... Chargement ...");
                java.lang.System.out.println("Le compte de l'employé est créé. Retour au menu\n");
            } else { //bénévole
                java.lang.System.out.println("... Chargement ...");
                java.lang.System.out.println("Le compte du bénévole est créé. Retour au menu\n");
            }
            menu();
        }
    }*/

    /*public static void updateCompte(){
        java.lang.System.out.println("\n   ---   Mise-à-jour d'un compte   ---   \n");
        Scanner scanner = new Scanner(java.lang.System.in);
        java.lang.System.out.println("visiteur ou personnel ? (respecter l'orthographe et la casse)");
        String typeCompte = scanner.nextLine();
        java.lang.System.out.println("Numéro du compte ?");
        String compte = scanner.nextLine();
         if (typeCompte.equals("visiteur")){
            creerCompteVisiteur();
        } else if (typeCompte.equals("personnel")){
            creerComptePersonnel();
        } else{
            System.out.println("poste non reconnu");
            System.out.println("Retour au menu\n");
            menu();
        }
        String choix = "";
        String change = "";
        if (typeCompte.equals("visiteur")){
            java.lang.System.out.println("Sélectionner le changement voulu :");
            java.lang.System.out.println(" 1: changement de numéro de téléphone");
            java.lang.System.out.println(" 2: changement de courriel");
            java.lang.System.out.println(" 3: ajout d'une réservation");
            choix = scanner.nextLine();
            switch (choix){
                case "1" -> {
                    java.lang.System.out.println("Nouveau numéro de téléphone ?");
                    change = scanner.nextLine();
                }
                case "2" -> {
                    java.lang.System.out.println("Nouveau courriel ?");
                    change = scanner.nextLine();
                }
                case "3" -> {
                    java.lang.System.out.println("Nouvelle réservation ?");
                    change = scanner.nextLine();
                }
            }
        } else if (typeCompte.equals("personnel")){
            java.lang.System.out.println("Sélectionner le changement voulu :");
            java.lang.System.out.println(" 1: changement de numéro de téléphone");
            java.lang.System.out.println(" 2: changement de courriel");
            java.lang.System.out.println(" 3: changement d'adresse");
            java.lang.System.out.println(" 4: changement des disponibilités");
            java.lang.System.out.println();
            choix = scanner.nextLine();
            switch (choix){
                case "1" -> {
                    java.lang.System.out.println("Nouveau numéro de téléphone ?");
                    change = scanner.nextLine();
                }
                case "2" -> {
                    java.lang.System.out.println("Nouveau courriel ?");
                    change = scanner.nextLine();
                }
                case "3" -> {
                    java.lang.System.out.println("Nouvelle adresse ?");
                    change = scanner.nextLine();
                }
                case "4" -> {
                    java.lang.System.out.println("Nouvelles disponibilités");
                    change = scanner.nextLine();
                }
            }
        }
        java.lang.System.out.println("\n... Chargement ...");
        if (change.length() > 0){
            java.lang.System.out.println("La nouvelle information est : " + change);
            java.lang.System.out.println("Est-ce bien correct ? (oui ou non)");
            String valide = scanner.nextLine();
            if (valide.equals("non")){
                java.lang.System.out.println("Aucune modification n'a été enregistrée. Retour au menu\n");
                menu();
            } else{
                java.lang.System.out.println("... Chargement ...");
                java.lang.System.out.println("La modification a été enregistrée. Retour au menu\n");
                menu();
            }
        } else{
            java.lang.System.out.println("Aucune modification n'a été enregistrée. Retour au menu\n");
            menu();
        }
    }*/
}
