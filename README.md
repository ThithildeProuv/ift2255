# IFT2255: Projet Vax-Todo:Re

Bienvenue sur le repo de Mathématrix pour le projet du cours de génie logiciel.

## Le projet VaxTodo:Re

Dans le contexte de la pandémie de la Covid-19, une grande campagne de vaccination a lieu. Pour aider, la compagnie GoodPeople a ouvert un local avec des bénévoles, employés et professionnels de la santé afin de vacciner des gens. Notre but est de concevoir un logiciel permettant de gérer les rendez-vous : prise de rendez-vous, création de compte d'utilisateurs, envoi de rapport de visite avec preuve de vaccination, gestion des bénévoles, etc.

## Installation

Une fois cloné, ce repo n'a besoin que de runner du Java (du moins pour l'instant).  

## L'équipe Mathématrix

* Mathilde Prouvost
* Elizabeth Michel
* Guillaume Pilon

## Contenu du repo

### Fichiers du logiciel

* src

### Fichiers en lien avec le projet

* Entretiens avec le client
* Echéancier
* Glossaire
* Scénarios

#### Diagrammes UML

* Diagramme des cas d'utilisation
* Diagrammes d'activité
* Diagrammes de classes
* Diagrammes de séquences

### Fichiers en lien avec les devoirs

* Rapports
* Fichiers HTML et CSS
