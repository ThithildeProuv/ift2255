import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        menu();
    }

    public static void menu(){
        System.out.println("\n   ---  Menu  ---   \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("1: Prendre un rendez-vous");
        System.out.println("2: Confirmer un rendez-vous (présence du visiteur)");
        System.out.println("3: Créer un compte visiteur");
        System.out.println("4: Créer un compte de personnel");
        System.out.println("5: Mettre à jour un compte");
        System.out.println("q: Quitter");
        String choix = scanner.nextLine();

        switch (choix) {
            case "q" -> System.exit(0);
            case "1" -> prendreRdv();
            case "2" -> confirmerRdv();
            case "3" -> creerCompteVisiteur();
            case "4" -> creerComptePersonnel();
            case "5" -> updateCompte();
            default -> {
                System.out.println("Commande non reconnue");
                menu();
            }
        }
        scanner.close();
    }

    public static void prendreRdv(){
        System.out.println("\n   ---   Prendre un rdv   ---   \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Accès au calendrier ...");
        System.out.println("Choisir parmis les jours suivants:\t\t 1. jour 1\t\t 2. jour 2\t\t 3. jour 3");
        String jour = scanner.nextLine();
        System.out.println("Choisir parmis les horaires suivants:\t 1. horaire 1\t 2. horaire 2\t 3. horaire 3");
        String heure = scanner.nextLine();
        //System.out.println("L'horaire est le " + jour + " à " + heure + "h");  // Output user input
        System.out.println("Prénom ?");
        String prenom = scanner.nextLine();
        System.out.println("Nom ?");
        String nom = scanner.nextLine();
        String name = prenom + " " + nom;
        //System.out.println("Vous êtes " + name);
        System.out.println("Type de dose (1 ou 2) ?");
        String dose = scanner.nextLine();
        //System.out.println("C'est votre dose n°" + dose);
        System.out.println("Adresse courriel ?");
        String courriel = scanner.nextLine();

        System.out.println("\n... Chargement ...");
        System.out.println("Résumé : rendez-vous pour " + name + " contactable par " + courriel + " pour sa dose n° " + dose + " le " + jour + " à " + heure + "h");
        System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            prendreRdv();
        } else {
            Random random = new Random();
            String num = Integer.toString(random.nextInt(1000000000));
            System.out.println("Le numéro unique de la réservation est " + num);
            System.out.println("... Chargement ...");
            System.out.println("Le rendez-vous est réservé. Retour au menu\n");
            menu();
        }
        scanner.close();
    }

    public static void confirmerRdv(){
        System.out.println("\n   ---   Confirmation de rendez-vous   ---   \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Numéro de réservation ? (x pour inconnu)");
        String num = scanner.nextLine();
        String name = "Louis-Edouard Lafontant";
        if (num.equals("x")){
            System.out.println("Prénom ?");
            String prenom = scanner.nextLine();
            System.out.println("Nom ?");
            String nom = scanner.nextLine();
            name = prenom + " " + nom;
            Random random = new Random();
            num = Integer.toString(random.nextInt(1000000000));
        }
        //System.out.println("Vous êtes" + name);
        System.out.println("Heure du rdv ?");
        String heure = scanner.nextLine();

        System.out.println("\n... Chargement ...");
        System.out.println("Résumé : rendez-vous " + num + " de " + name + " à " + heure + "h");
        System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            confirmerRdv();
        } else {
            System.out.println("... Chargement ...");
            System.out.println("Le rendez-vous est confirmé. Retour au menu\n");
            menu();
        }
        scanner.close();
    }

    public static void creerCompteVisiteur(){
        System.out.println("\n   ---   Création d'un compte de visiteur   ---   \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Prénom ?");
        String prenom = scanner.nextLine();
        System.out.println("Nom ?");
        String nom = scanner.nextLine();
        String name = prenom + " " + nom;
        System.out.println("Date de naissance (AAAA-MM-JJ) ?");
        String birthday = scanner.nextLine();
        System.out.println("Numéro de téléphone ?");
        String cellNumber = scanner.nextLine();
        System.out.println("Adresse courriel ?");
        String courriel = scanner.nextLine();
        System.out.println("Numéros de réservation s'il y en a ? (laisser vide sinon)");
        String reservations = scanner.nextLine();

        System.out.println("\n... Chargement ...");
        System.out.println("Résumé : visiteur " + name + ", né le " + birthday + ", contactable au " + cellNumber + " ou par " + courriel);
        if (reservations.length()>0) {
            System.out.println("Ses réservations sont : " + reservations);
        }
        System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            creerCompteVisiteur();
        } else {
            Random random = new Random();
            String num = Integer.toString(random.nextInt(1000000000));
            System.out.println("Le numéro unique du compte est " + num);
            System.out.println("... Chargement ...");
            System.out.println("Le compte du visiteur est créé. Retour au menu\n");
            menu();
        }
        scanner.close();
    }

    public static void creerComptePersonnel(){
        System.out.println("\n   ---   Création d'un compte de personnel   ---   \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Prénom ?");
        String prenom = scanner.nextLine();
        System.out.println("Nom ?");
        String nom = scanner.nextLine();
        String name = prenom + " " + nom;
        System.out.println("Numéro de téléphone ?");
        String cellNumber = scanner.nextLine();
        System.out.println("Adresse postale ?");
        scanner.nextLine();// String adresse = scanner.nextLine();
        System.out.println("Adresse courriel ?");
        String courriel = scanner.nextLine();
        System.out.println("employé ou bénévole ? (respecter l'orthographe, les accents et la casse)");
        String poste = scanner.nextLine();
        System.out.println("Jours de disponibilité ? (séparés par des espaces)");
        String dispos = scanner.nextLine();

        System.out.println("\n... Chargement ...");
        System.out.println("Résumé : " + poste + " " + name + ", contactable au " + cellNumber + " ou par " + courriel);
        System.out.println("Disponible les " + dispos);
        System.out.println("Les informations sont-elles correctes ? (oui ou non)");
        String valide = scanner.nextLine();
        if (valide.equals("non")){ //on recommence
            creerComptePersonnel();
        } else {
            Random random = new Random();
            String num = Integer.toString(random.nextInt(1000000000));
            System.out.println("Le numéro unique du compte est " + num);
            if (poste.equals("employé")){
                String numEmploye = Integer.toString(random.nextInt(1000000));
                System.out.println("Le code de l'employé est " + numEmploye);
                System.out.println("Choix du mot de passe");
                String password = scanner.nextLine();
                boolean passwordValide = password.length() >= 8;
                while (!passwordValide){
                    System.out.println("Mot de passe non valide");
                    System.out.println("Choix du mot de passe (au moins 8 caractères)");
                    password = scanner.nextLine();
                    passwordValide = password.length() >= 8;
                }
                System.out.println("... Chargement ...");
                System.out.println("Le compte de l'employé est créé. Retour au menu\n");
            } else { //bénévole
                System.out.println("... Chargement ...");
                System.out.println("Le compte du bénévole est créé. Retour au menu\n");
            }
            menu();
        }
        scanner.close();
    }

    public static void updateCompte(){
        System.out.println("\n   ---   Mise-à-jour d'un compte   ---   \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println("visiteur ou personnel ? (respecter l'orthographe et la casse)");
        String typeCompte = scanner.nextLine();
        System.out.println("Numéro du compte ?");
        scanner.nextLine();// String compte = scanner.nextLine();
        /* if (typeCompte.equals("visiteur")){
            creerCompteVisiteur();
        } else if (typeCompte.equals("personnel")){
            creerComptePersonnel();
        } else{
            System.out.println("poste non reconnu");
            System.out.println("Retour au menu\n");
            menu();
        }*/
        String choix = "";
        String change = "";
        if (typeCompte.equals("visiteur")){
            System.out.println("Sélectionner le changement voulu :");
            System.out.println(" 1: changement de numéro de téléphone");
            System.out.println(" 2: changement de courriel");
            System.out.println(" 3: ajout d'une réservation");
            choix = scanner.nextLine();
            switch (choix){
                case "1" -> {
                    System.out.println("Nouveau numéro de téléphone ?");
                    change = scanner.nextLine();
                }
                case "2" -> {
                    System.out.println("Nouveau courriel ?");
                    change = scanner.nextLine();
                }
                case "3" -> {
                    System.out.println("Nouvelle réservation ?");
                    change = scanner.nextLine();
                }
            }
        } else if (typeCompte.equals("personnel")){
            System.out.println("Sélectionner le changement voulu :");
            System.out.println(" 1: changement de numéro de téléphone");
            System.out.println(" 2: changement de courriel");
            System.out.println(" 3: changement d'adresse");
            System.out.println(" 4: changement des disponibilités");
            System.out.println();
            choix = scanner.nextLine();
            switch (choix){
                case "1" -> {
                    System.out.println("Nouveau numéro de téléphone ?");
                    change = scanner.nextLine();
                }
                case "2" -> {
                    System.out.println("Nouveau courriel ?");
                    change = scanner.nextLine();
                }
                case "3" -> {
                    System.out.println("Nouvelle adresse ?");
                    change = scanner.nextLine();
                }
                case "4" -> {
                    System.out.println("Nouvelles disponibilités");
                    change = scanner.nextLine();
                }
            }
        }
        System.out.println("\n... Chargement ...");
        if (change.length() > 0){
            System.out.println("La nouvelle information est : " + change);
            System.out.println("Est-ce bien correct ? (oui ou non)");
            String valide = scanner.nextLine();
            if (valide.equals("non")){
                System.out.println("Aucune modification n'a été enregistrée. Retour au menu\n");
                menu();
            } else{
                System.out.println("... Chargement ...");
                System.out.println("La modification a été enregistrée. Retour au menu\n");
                menu();
            }
        } else{
            System.out.println("Aucune modification n'a été enregistrée. Retour au menu\n");
            menu();
        }
        scanner.close();
    }
}
