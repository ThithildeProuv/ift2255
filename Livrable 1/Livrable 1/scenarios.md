# Les scénarios

## Scénario principal d'une prise de rendez-vous

1. Le visiteur appelle un employé (par téléphone)

2. L'employé utilise le système pour accéder au calendrier et vérifier les disponibilités.

3. Le visiteur indique l'horaire qui lui convient.

4. L'employé demande au visiteur ses informations personnelles:

    4.1 prénom et nom

    4.2 courriel

    4.3 type de dose (1ère ou 2ème)

5. Le système génère un numéro unique de réservation associé à la visite

6. Le système inscrit le visiteur à l'horaire.

    6.1 Le système envoie un courriel au visiteur avec son numéro de réservation et ses informations.

    6.2 Le système enregistre les informations du visiteur et l'horaire (date et heure) de son rendez-vous.

## Scénario principal d'une visite (avec rendez-vous)

0. 48h avant le rendez-vous, l'employé appelle le visiteur pour lui rappeler son rendez-vous.

1. Un visiteur avec un rendez-vous arrive sur le lieu de vaccination.

2. Un bénévole vérifie qu'il a son rendez-vous.

    2.1 Le bénévole demande son numéro de réservation ou son nom et l'heure de son rendez-vous au visiteur

3. Le bénévole invite le visiteur à se rendre à la file des visites planifiées.

4. Le visiteur attend son tour.

5. A son tour, le visiteur va vers un employé.

6. L'employé confirme son rendez-vous.

    6.1 L'employé demande au visiteur son numéro de réservation ou son nom et l'heure de son rendez-vous.

7. L'employé demande ses informations au visiteur et remplis un questionnaire

    7.1 carte d'assurance maladie

    7.2 but de la visite (1ère ou 2ème dose)

    7.3 déjà contracté la covid

    7.4 symptômes de la covid

    7.5 allergies

    7.6 quel type de vaccin

8. Le visiteur répond que c'est sa 1ère dose et répond à toutes les questions.

9. L'employé crée un compte pour le visiteur.

    9.1 Le système génère un numéro unique du compte

10. L'employé lui demande s'il veut planifier une 2ème dose.

11. Le visiteur veut planifier une 2ème dose.

12. L'employé inscrit un 2ème rendez-vous avec le visiteur

    12.1 L'employé et le visiteur conviennent d'une date

    12.2 L'employé demande et enregistre le nom complet et le numéro de téléphone

    12.3 L'employé enregistre le jour et l'horaire du rendez-vous

    12.4 Le système génère un numéro unique de reservation.

13. Le système attribue un numéro dans la file d'attente.

    13.1 L'employé donne le numéro au visiteur

14. Le visiteur attend dans une nouvelle file d'attente.

15. A son tour, un employé appelle le visiteur.

    15.1 Le visiteur est dirigé vers un professionnel de la santé.

16. Le professionnel de la santé vérifie qu'il a le bon visiteur.

17. Le professionnel de la santé vaccine le visiteur.

18. Le visiteur s'en va.

19. A la fin de la journée, le système envoie au visiteur les détails de sa visite.

    19.1 Le système envoie sa preuve de vaccination.

## Scénario alternatif pour visite spontanée (ou retard)

1.a Le visiteur arrive sans rendez-vous.

2.a Le bénévole crée un rendez-vous.

    2.1.a Il demande son nom

    2.2.a Le système vérifie qu'il y a une place disponible

    2.3.a Le système génère un numéro unique de reservation

3.a Le bénévole invite le visiteur à se rendre à la file des visites spontanées.

## Scénario alternatif pour 2ème visite

8.b Le patient répond que c'est sa 2ème dose et répond à toutes les questions.

9.b L'employé vérifie le compte du visiteur.

On enlève les étapes 10 à 13.

-----

## Gestion des rdv

* Prendre un 1er rendez-vous
* Gestion du suivi (prendre un 2ème rdv)
* Rappeler un rendez-vous
* Confirmer la présence au rdv
* // Déroulement de la visite
* Envoyer le détail du rdv
* Annuler un rdv

## Gestion des comptes et des personnes

* Créer un compte visiteur (et un bénévole et un employé et un prof. de la santé)
* Mettre à jour un compte
* Gestion des bénévoles
* // Gestion des professionnels de la santé
