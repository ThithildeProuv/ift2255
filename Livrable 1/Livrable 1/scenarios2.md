# Scenarios

## Gestion des visites
Nom: Prendre un rendez-vous.

But: Un visiteur désire prendre un rendez-vous pour se faire vacciner.

Acteurs: Employé (principal), visiteur

Préconditions: Le visiteur appelle un employé au téléphone

### Prendre un 1er rdv

1. Le visiteur appelle un employé (par téléphone)
2. L'employé utilise le système pour accéder au calendrier et vérifier les disponibilités.
3. Le visiteur indique l'horaire qui lui convient.
4. L'employé demande au visiteur ses informations personnelles:
    1. prénom et nom
    2. courriel
    3. type de dose (1ère ou 2ème)
5. Le système génère un numéro unique de réservation associé à la visite
6. Le système inscrit le visiteur à l'horaire.
    1. Le système envoie un courriel au visiteur avec son numéro de réservation et ses informations.
    2. Le système enregistre les informations du visiteur et l'horaire (date et heure) de son rendez-vous.

Postconditions: Le rendez-vous est conservé dans les données du système.

### Alternatif: Prendre un 2ème rdv (gestion du suivi)

1.a La prise du 2ème rendez-vous peut se prendre sur place pendant le 1er rdv  
4.a L'employé vérifie avec le visiteur ses informations personnelles  
4.a.1. ajouter le numéro du compte du visiteur

### Rappeler un rdv
Nom: Rappeler un rendez-vous.

But: Le système rappelle au visiteur son rendez-vous 48 heures à l'avance.

Acteurs: Visiteur (principal)

Préconditions: Le visiteur avait pris un rdv au moins 72h avant son rdv.

1. Le visiteur avait pris un rdv au moins 72h avant son rdv.
2. 48h avant son rdv, le système envoie un courriel au visiteur.
    1. Le courriel contient les informations du visiteur et l'horaire de son rdv

### Confirmer la présence au rdv

Nom: Confirmer la présence à un rendez-vous.

But: L'employé confirme la présence du visiteur à son rendez-vous.

Acteurs: Employé (principal)

Préconditions: Le visiteur se présente à l'employé en personne au lieu de vaccination.

1. Le visiteur se présente à l'employé
2. L'employé lui demande son numéro de réservation ou son nom et ses informations.
3. L'employé enregistre la présence du visiteur dans le système.

Postconditions: Le rendez-vous est confirmé dans le système.

### Alternatif: annuler un rdv (en cas d'absence ou de retard)

1.a Le visiteur n'est pas là (ou en retard)  
2.a Le rendez-vous est annulé

### Déroulement d'une visite (plannifiée) (1ère visite)

Nom: Visite pour la vaccination.

But: Le visiteur se fait vacciner.

Acteurs: Employé (principal), visiteur

Préconditions: Le visiteur se présente au centre de vaccination à l'heure de son rendez-vous.

1. Le visiteur se présente à un bénévole.
2. Le bénévole vérifie qu'il a un rdv
    1. Le bénévole demande son numéro de réservation ou son nom et l'heure de son rendez-vous au visiteur
3. Le bénévole dirige le visiteur vers la file des visites planifiées.
4. Le visiteur attend son tour.
5. A son tour, le visiteur va vers un employé.
6. L'employé confirme son rendez-vous.
    * cf scénario confirmation de rdv
7. L'employé demande si c'est la première visite (compte créé ?)
8. Le visiteur répond que c'est sa 1ère visite.
9. L'employé crée un compte pour le visiteur
    1. L'employé pose les questions du formulaire au visiteur
    2. Le visiteur répond aux questions
    * cf scénario création de compte
10. L'employé demande au visiteur s'il veut planifier une 2ème visite.
11. Le visiteur veut planifier une 2ème visite.
12. L'employé inscrit un 2ème rdv avec le visiteur.
    * cf scénario gestion du suivi (prendre 2ème rdv)
13. Le système attribue un numéro de file d'attente.
    1. L'employé donne le numéro au visiteur
14. Le visiteur attend dans une nouvelle file d'attente.
15. A son tour, un employé appelle le visiteur.
    1. Le visiteur est dirigé vers un professionnel de la santé.
16. Le professionnel de la santé vérifie qu'il a le bon visiteur.
17. Le professionnel de la santé vaccine le visiteur.
18. Le visiteur s'en va.

Postconditions: Le compte du visiteur est créé dans le cas d'une première visite et
les informations par rapport à la visite sont conservées dans les données du système.

### Alternatif: déroulement d'une visite (2ème visite)

8.a Le visiteur répond que c'est sa 2ème visite.  
9.a L'employé utilise le numéro de compte du visiteur.  
On enlève les étapes 10 à 12.

### Alternatif: déroulement d'une visite spontannée (ou retard)

2.1.a Le visiteur répond qu'il n'a pas de rdv  
2.2.a Le bénévole vérifie qu'il y a un créneau disponible  
3.a Le bénévole dirige le visiteur vers la file des visites spontannées.

### Envoyer le détail du rdv
Nom: Rappeler un rendez-vous.

But: Le système envoie au visiteur le détail de son rendez-vous la journée de sa visite.

Acteurs: Visiteur (principal)

Préconditions: Le visiteur a reçu son vaccin.

1. Le système envoie un courriel au visiteur avec
    1. le détail de sa visite
    2. la preuve de vaccination sous forme de PDF avec nom, prénom, date de naissance, QR Code et liste des vaccins administrés.

### Fin de journée

1. Le système accède à la liste de tous les visiteurs de la journée.
2. Le système envoie à chacun d'entre eux leur preuve de vaccination.
    * cf scénario Envoyer le détail du rdv
3. Le système accède à la liste des réservations des rendez-vous du sur-lendemain.
4. Le système envoie à chacun d'entre eux un rappel de rdv.
    * cf scénario Rappeler un rdv

## Gestion des comptes

### Créer un compte de visiteur

Nom: Créer un compte de visiteur.

But: Un employé crée un nouveau compte pour un visiteur.

Acteurs: Employé (principal), visiteur

Préconditions: Le visiteur est présent à son rendez-vous.

1. L'employé demande au visiteur ses informations personnelles
2. Le visiteur lui répond
3. L'employé note les informations du patient dans le système
4. Le système enregistre toutes les données.
5. Le système génère un numéro unique de compte.
    1. L'employé donne son numéro de compte au visiteur.

Postconditions: Les informations du visiteur sont conservées dans les données du système.

### Gestion des bénévoles

Nom: Gestion des bénévoles

But: Une liste est imprimée à partir des rendez-vous du système pour que les bénévoles puissent
vérifier les rendez-vous des visiteurs à leur accueil et gérer la disponibilité des bénévoles.

Acteurs: Employé (principal)

Préconditions: Les bénévoles ont entré leurs disponibilités et les visiteurs ont pris des rendez-vous.

1. Au début de la journée, l'employé imprime une liste des réservation pour chaque bénévole
2. L'employé confirme la présence de chaque bénévole.
